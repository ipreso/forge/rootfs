��     0   �  {  @     ,     4  T      �  �   �  �   �  )   M     w  �   �  �   �  :     O   J  B   �  C   �     !  �   9  �     �   �  3   �  }   �  I   z   L   �   �   !  �   �!  �   �"  �   P#  O   �#  >   ?$  �   ~$  I   S%  D   �%  E   �%     (&  0   <&  H   m&  N   �&  7   '  "   ='  '   `'  |   �'     (      (  t   =(  $   �(  "   �(     �(  ,   )  ,   C)  ,   p)  '   �)  -   �)      �)  (   *  (   =*     f*     *     �*     �*  *   �*  *   +     1+     3+  _   7+  g   �+  g   �+  h   g,     �,     �,  1   �,     &-  #   B-     f-  $   �-  $   �-  !   �-  !   �-  -   .     <.  !   \.  &   ~.      �.  <   �.     /  >   /     Q/     i/  +   �/     �/  )   �/  @   �/  $   :0     _0  "   ~0     �0     �0  �   �0  �   f1  I   2    d2  ?   s3  9   �3  #   �3  8   4  1   J4     |4      �4  <   �4  b   �4  b   Z5  c   �5  �   !6     �6     �6     �6  >   �6  t   7     �7  6   �7  7   �7  g   8     8     �8  4   �8     �8  �   �8  M   �9  +   �9  3   ":  W   V:  x   �:     ';  ?   E;  �   �;  X   1<  <   �<  )   �<  "   �<  �   =  3   >  6   C>  9   z>  (   �>  �   �>  �   o?  D   @     Y@     t@  7   �@     �@  [   �@  -   ;A  '   iA  �  �A  0   1C     bC  *   wC  �   �C  /   -D  *   ]D  2   �D  4   �D  A   �D  o   2E     �E  ;   �E  3   �E  /   )F  +   YF  '   �F  #   �F     �F     �F     G  q   G  :   �G  �   �G     vH     �H     �H     �H     �H     �H     �H  7   �H  =   !I  C   _I  3   �I     �I  :   �I     #J     /J     @J     ^J  !   oJ     �J     �J  !   �J  Y   �J     K     ,K     IK     ^K  !   ~K  =   �K  !   �K  +    L     ,L      LL      mL  #   �L  6   �L  -   �L  $   M  %   <M  '   bM     �M     �M     �M     �M     �M     N  P   N  6   pN  n   �N  )   O  P   @O     �O     �O  3   �O     �O  $   P  D   0P  (   uP  �   �P     'Q     8Q  )   QQ  %   {Q      �Q  �   �Q  8   �R  !   �R  	  �R  L   �S  6   CT     zT  *   �T     �T     �T  )  �T     �U     V  *   'V     RV     gV     �V     �V     �V  W   �V  ?   W  I  YW  C   �X  .   �X  H   Y  +   _Y  :   �Y  q   �Y  >   8Z  o   wZ  "   �Z  -   
[  @   8[  �   y[  �   [\     A]     M]  �  c]  �   P_  �   �_  �   �`  e   Aa  �   �a  z   �b  A   c  ~   [c  O   �c  3   *d     ^d  �   wd    Se  �   nf  4   Zg  �   �g  Q   )h  |   {h     �h  �   j  �   �j  �   �k  }   Ll  <   �l  �   m  R   �m  B   �m  N   :n     �n  5   �n  F   �n  N   o  0   mo  !   �o     �o  h   �o     Fp  #   bp  �   �p  $   q  &   2q      Yq  +   zq  +   �q  +   �q  %   �q  ,   $r      Qr  &   rr  &   �r     �r     �r  !   �r  !   s  ,   ?s  ,   ls     �s     �s  Z   �s  c   �s  k   ^t  g   �t     2u     Cu  0   Zu  %   �u  /   �u  %   �u  1   v  >   9v  0   xv  0   �v  7   �v      w     3w  :   Nw      �w  E   �w     �w  =   �w     =x  &   ]x  :   �x  &   �x  &   �x  D   y  *   Ry     }y  %   �y  /   �y     �y  �   
z  �   �z  V   H{  �  �{  F   7}  0   ~}  =   �}  ;   �}  3   )~     ]~  %   }~  F   �~  k   �~  k   V  d   �  �   '�      ��     Ӏ     �  �   �  �   w�      ��  I   �  ?   h�  n   ��     �     ,�  6   F�  s   }�  �   �  S   ل  1   -�  /   _�  h   ��  f   ��     _�  :   ~�  �   ��  \   l�  7   ɇ  *   �  &   ,�  �   S�  6   E�  3   |�  (   ��  %   ى  `   ��  �   `�  :   	�      D�  $   e�  6   ��     ��  ^   ڋ  /   9�  (   i�  �  ��  5   =�     s�  8   ��  �   Î  :   F�  7   ��  ;   ��  :   ��  N   0�  ^   �     ސ  4   �  -   '�  *   U�  &   ��  "   ��     ʑ     �     �     �  r   .�  <   ��  �   ޒ     ��     ��     ��     ��     ��     ɓ     �  D   �  P   K�  I   ��  ?   �     &�  D   >�     ��     ��  /   ��     ޕ  -   �     �      �  "   '�  k   J�     ��     Ԗ     �  '   �  $   4�  H   Y�  $   ��  '   Ǘ  &   �  %   �  %   <�  &   b�  9   ��  +   Ø  %   �  ,   �  '   B�     j�  "   ��     ��     Ǚ  $   �     	�  ,   �  0   G�  j   x�  *   �  S   �     b�  $   w�  2   ��     ϛ  +   �  P   �  /   g�  �   ��     &�     :�  6   W�  #   ��     ��  �   ѝ  @   ��  *   �  V  �  \   u�  @   Ҡ     �  *   �     E�     `�  *  q�     ��     ��  2   ڢ     �     $�  %   C�     i�     q�  _   ��  F   �  A  /�  L   q�  /   ��  C   �  *   2�  A   ]�  d   ��  H   �  p   M�  1   ��  3   �  N   $�  �   s�  �   +�     �     ��     �          H   �   �          �   �   s   {   +   �   �   R       �         B      �   �   �   #           �   r   �   j       
  �   h   6   u   :   �        �       e   �   Z   �          @   v         }   c   �                       \   �   �   |                  p   M   �   �   �   A           �   �   �   <   �   �   �   C   "   3   �      �   �             w   ?          �       �   I     1       �       �   !       �   �              [   �   �   f       �   9   �   x   U      X           (   P              �   
       )   W           �       �   k   ^   �   �          �   �         D         �       �   �         .         �   �      �       4   *   �   �       �   �       y      o   =   �   ~       �   J      /       �   �     -   >       �   	       �                     i   F   �   �   �   K   �   �   �   n                   _   �       �      �          �   N   �     G          �   �   �   �   �               8       �       �   �   �   Q       5           �           	          &   %   �       t   �   �       �   �       �   �       �       Y       a   g   �   `   �       �                �         �   �   �   �   E      �   �                   q   �          ]   �   S                 �   7   �   �         �   �           �              �   T           �   �       �   �     �   z           O   2   L       �   V   �   �   m   �   d           �     l   ;   0   $   �   �       '       �   b          �              ,      �  t  �  �  �  �  �  �    $  @  \  p  �  �  �  �   �  7              $   ������  =              �   ������  $          ����֫  5          �����  H          ����W�  2          ������  /          ������  7          ������  @              +   ����x�  H              �   ������  %       	   ����ݮ  :          �����  W          ����r�  2          ������  >          �����  @          ���� 
Execution of xargs will continue now, and it will try to read its input and run commands; if this is not what you wanted to happen, please type the end-of-file keystroke.
 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to <bug-findutils@gnu.org>.
 
Report bugs to: %s
 
actions: -delete -print0 -printf FORMAT -fprintf FILE FORMAT -print 
      -fprint0 FILE -fprint FILE -ls -fls FILE -prune -quit
      -exec COMMAND ; -exec COMMAND {} + -ok COMMAND ;
      -execdir COMMAND ; -execdir COMMAND {} + -okdir COMMAND ;
 
default path is the current directory; default expression is -print
expression may consist of: operators, options, tests, and actions:
       --help                   display this help and exit
       --process-slot-var=VAR   set environment variable VAR in child processes
       --show-limits            show limits on command-line length
       --version                output version information and exit
       -context CONTEXT
       -nouser -nogroup -path PATTERN -perm [-/]MODE -regex PATTERN
      -readable -writable -executable
      -wholename PATTERN -size N[bcwkMG] -true -type [bcdpflsD] -uid N
      -used N -user NAME -xtype [bcdpfls]   -0, --null                   items are separated by a null, not whitespace;
                                 disables quote and backslash processing and
                                 logical EOF processing
   -E END                       set logical EOF string; if END occurs as a line
                                 of input, the rest of the input is ignored
                                 (ignored if -0 or -d was specified)
   -I R                         same as --replace=R
   -L, --max-lines=MAX-LINES    use at most MAX-LINES non-blank input lines per
                                 command line
   -P, --max-procs=MAX-PROCS    run at most MAX-PROCS processes at a time
   -a, --arg-file=FILE          read arguments from FILE, not standard input
   -d, --delimiter=CHARACTER    items in input stream are separated by CHARACTER,
                                 not by whitespace; disables quote and backslash
                                 processing and logical EOF processing
   -e, --eof[=END]              equivalent to -E END if END is specified;
                                 otherwise, there is no end-of-file string
   -i, --replace[=R]            replace R in INITIAL-ARGS with names read
                                 from standard input; if R is unspecified,
                                 assume {}
   -l[MAX-LINES]                similar to -L but defaults to at most one non-
                                 blank input line if MAX-LINES is not specified
   -n, --max-args=MAX-ARGS      use at most MAX-ARGS arguments per command line
   -p, --interactive            prompt before running commands
   -r, --no-run-if-empty        if there are no arguments, then do not run COMMAND;
                                 if this option is not given, COMMAND will be
                                 run at least once
   -s, --max-chars=MAX-CHARS    limit length of command line to MAX-CHARS
   -t, --verbose                print commands before executing them
   -x, --exit                   exit if the size (see -s) is exceeded
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s is an slocate database of unsupported security level %d; skipping it. %s is an slocate database.  Support for these is new, expect problems for now. %s is an slocate database.  Turning on the '-e' option. %s is not the name of a known user %s is not the name of an existing group %s is not the name of an existing group and it does not look like a numeric group ID because it has the unexpected suffix %s %s terminated by signal %d %s%s argument '%s' too large %s%s changed during execution of %s (old device number %ld, new device number %ld, file system type is %s) [ref %ld] %s: exited with status 255; aborting %s: invalid number for -%c option
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: stopped by signal %d %s: terminated by signal %d %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 %s: value for -%c option should be <= %ld
 %s: value for -%c option should be >= %ld
 ' (C) -type %c is not supported because FIFOs are not supported on the platform find was compiled on. -type %c is not supported because Solaris doors are not supported on the platform find was compiled on. -type %c is not supported because named sockets are not supported on the platform find was compiled on. -type %c is not supported because symbolic links are not supported on the platform find was compiled on. < %s ... %s > ?  All Filenames: %s
 Arguments to -type should contain only one letter Cannot close standard input Cannot obtain birth time of file %s Cannot open input file %s Cannot read list of mounted devices. Cannot read mounted file system list Cannot set SIGUSR1 signal handler Cannot set SIGUSR2 signal handler Compression ratio %4.2f%% (higher is better)
 Compression ratio is undefined
 Database %s is in the %s format.
 Database was last modified at %s.%09ld Empty argument to the -D option. Environment variable %s is not set to a valid decimal number Eric B. Decker Expected a positive decimal integer argument to %s, but got %s Expected an integer: %s Failed to fully drop privileges Failed to initialize shared-file hash table Failed to read from stdin Failed to safely change directory into %s Failed to save working directory in order to run a command on %s Failed to write output (at stage %d) Failed to write prompt for -ok Failed to write to standard output Failed to write to stderr Features enabled:  File descriptor %d will leak; please report this as a bug, remembering to include a detailed description of the simplest way to reproduce this problem. File names have a cumulative length of %s bytes.
Of those file names,

	%s contain whitespace, 
	%s contain newline characters, 
	and %s contain characters with the high bit set.
 File system loop detected; %s is part of the same file system loop as %s. Filesystem loop detected; %s has the same device number and inode as a directory which is %d level higher in the file system hierarchy Filesystem loop detected; %s has the same device number and inode as a directory which is %d levels higher in the file system hierarchy General help using GNU software: <http://www.gnu.org/gethelp/>
 I cannot figure out how to interpret %s as a date or time Ignoring unrecognised debug flag %s In %s the %s must appear by itself, but you specified %s Invalid argument %s for option --max-database-age Invalid argument %s to -used Invalid argument `%s%s' to -size Invalid escape sequence %s in input delimiter specification. Invalid escape sequence %s in input delimiter specification; character values must not exceed %lo. Invalid escape sequence %s in input delimiter specification; character values must not exceed %lx. Invalid escape sequence %s in input delimiter specification; trailing characters %s not recognised. Invalid input delimiter specification %s: the delimiter must be either a single character or an escape sequence starting with \. Invalid optimisation level %s James Youngman Kevin Dalley Locate database size: %s byte
 Locate database size: %s bytes
 Mandatory and optional arguments to long options are also
mandatory or optional for the corresponding short option.
 Matching Filenames: %s
 Old-format locate database %s is too short to be valid Only one instance of {} is supported with -exec%s ... + Optimisation level %lu is too high.  If you want to find files very quickly, consider using GNU locate. Packaged by %s
 Packaged by %s (%s)
 Please specify a decimal number immediately after -O Report %s bugs to: %s
 Report (and track progress on fixing) bugs via the findutils bug-reporting
page at http://savannah.gnu.org/ or, if you have no web access, by sending
email to <bug-findutils@gnu.org>. Run COMMAND with arguments INITIAL-ARGS and more arguments read from input.

 Security level %s has unexpected suffix %s. Security level %s is outside the convertible range. Some filenames may have been filtered out, so we cannot compute the compression ratio.
 Symbolic link %s is part of a loop in the directory hierarchy; we have already visited the directory to which it points. The %s test needs an argument The -O option must be immediately followed by a decimal integer The -delete action automatically turns on -depth, but -prune does nothing when -depth is in effect.  If you want to carry on anyway, just explicitly use the -depth option. The -show-control-chars option takes a single argument which must be 'literal' or 'safe' The argument for option --max-database-age must not be empty The argument to -user should not be empty The atexit library function failed The current directory is included in the PATH environment variable, which is insecure in combination with the %s action of find.  Please remove the current directory from your $PATH (that is, remove ".", doubled colons, or leading or trailing colons) The database has big-endian machine-word encoding.
 The database has little-endian machine-word encoding.
 The database machine-word encoding order is not obvious.
 The environment is too large for exec(). The environment variable FIND_BLOCK_SIZE is not supported, the only thing that affects the block size is the POSIXLY_CORRECT environment variable The relative path %s is included in the PATH environment variable, which is insecure in combination with the %s action of find.  Please remove that entry from $PATH This system does not provide a way to find the birth time of a file. Unexpected suffix %s on %s Unknown argument to -type: %c Unknown regular expression type %s; valid types are %s. Unknown system error Usage: %s [--version | --help]
or     %s most_common_bigrams < file-list > locate-database
 Usage: %s [-0 | --null] [--version] [--help]
 Usage: %s [-H] [-L] [-P] [-Olevel] [-D  Usage: %s [-d path | --database=path] [-e | -E | --[non-]existing]
      [-i | --ignore-case] [-w | --wholename] [-b | --basename] 
      [--limit=N | -l N] [-S | --statistics] [-0 | --null] [-c | --count]
      [-P | -H | --nofollow] [-L | --follow] [-m | --mmap] [-s | --stdio]
      [-A | --all] [-p | --print] [-r | --regex] [--regextype=TYPE]
      [--max-database-age D] [--version] [--help]
      pattern...
 Usage: %s [OPTION]... COMMAND [INITIAL-ARGS]...
 Valid arguments are: WARNING: Lost track of %lu child processes WARNING: a NUL character occurred in the input.  It cannot be passed through in the argument list.  Did you mean to use the --null option? WARNING: cannot determine birth time of file %s WARNING: file %s appears to have mode 0000 WARNING: file system %s has recently been mounted. WARNING: file system %s has recently been unmounted. WARNING: locate database %s was built with a different byte order Warning: %s will be run at least once.  If you do not want that to happen, then press the interrupt keystroke.
 Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 You may not use {} within the utility name for -execdir and -okdir, because this is a potential security problem. You need to specify a security level as a decimal integer. You specified the -E option, but that option cannot be used with slocate-format databases with a non-zero security level.  No results will be generated for this database.
 ] [path...] [expression]
 ^[nN] ^[yY] ` ambiguous argument %s for %s argument line too long argument list too long argument to -group is empty, but should be a group name arithmetic overflow when trying to calculate the end of today arithmetic overflow while converting %s days to a number of seconds can't call exec() due to argument size restrictions cannot delete %s cannot fit single argument within argument list size limit cannot fork cannot search %s cannot stat current directory command too long could not create pipe before fork days double environment is too large for exec errno-buffer safe_read failed in xargs_do_exec (this is probably a bug, please report it) error closing file error reading a word from %s error waiting for %s error waiting for child process error: %s at end of format string error: the format directive `%%%c' is reserved for future use expected an expression after '%s' expected an expression between '%s' and ')' failed to drop group privileges failed to drop setgid privileges failed to drop setuid privileges failed to open /dev/tty for reading failed to restore working directory after searching %s failed to return to initial working directory failed to return to parent directory failed to set environment variable %s failed to unset environment variable %s getfilecon failed: %s invalid %s%s argument '%s' invalid -size type `%c' invalid argument %s for %s invalid argument `%s' to `%s' invalid expression invalid expression; I was expecting to find a ')' somewhere but did not see one. invalid expression; empty parentheses are not allowed. invalid expression; expected to find a ')' but didn't see one.  Perhaps you need an extra predicate after '%s' invalid expression; you have too many ')' invalid expression; you have used a binary operator '%s' with nothing before it. invalid mode %s invalid null argument to -size invalid predicate -context: SELinux is not enabled. invalid predicate `%s' invalid suffix in %s%s argument '%s' locate database %s contains a filename longer than locate can handle locate database %s is corrupt or invalid locate database %s looks like an slocate database but it seems to have security level %c, which GNU findutils does not currently support memory exhausted missing argument to `%s' oops -- invalid default insertion of and! oops -- invalid expression type (%d)! oops -- invalid expression type! operators (decreasing precedence; -and is implicit where no others are given):
      ( EXPR )   ! EXPR   -not EXPR   EXPR1 -a EXPR2   EXPR1 -and EXPR2
      EXPR1 -o EXPR2   EXPR1 -or EXPR2   EXPR1 , EXPR2
 option --%s may not be set to a value which includes `=' paths must precede expression: %s positional options (always true): -daystart -follow -regextype

normal options (always true, specified before other expressions):
      -depth --help -maxdepth LEVELS -mindepth LEVELS -mount -noleaf
      --version -xdev -ignore_readdir_race -noignore_readdir_race
 read returned unexpected value %zu; this is probably a bug, please report it sanity check of the fnmatch() library function failed. single slocate security level %ld is unsupported. standard error standard output tests (N can be +N or -N or N): -amin N -anewer FILE -atime N -cmin N
      -cnewer FILE -ctime N -empty -false -fstype TYPE -gid N -group NAME
      -ilname PATTERN -iname PATTERN -inum N -iwholename PATTERN -iregex PATTERN
      -links N -lname PATTERN -mmin N -mtime N -name PATTERN -newer FILE time system call failed unable to allocate memory unable to record current working directory unexpected EOF in %s unexpected extra predicate unexpected extra predicate '%s' unknown unknown predicate `%s' unmatched %s quote; by default quotes are special to xargs unless you use the -0 option warning: -%s %s will not match anything because it ends with /. warning: Unix filenames usually don't contain slashes (though pathnames do).  That means that '%s %s' will probably evaluate to false all the time on this system.  You might find the '-wholename' test more useful, or perhaps '-samefile'.  Alternatively, if you are using GNU grep, you could use 'find ... -print0 | grep -FzZ %s'. warning: database %s is more than %d %s old (actual age is %.1f %s) warning: escape `\' followed by nothing at all warning: format directive `%%%c' should be followed by another character warning: not following the symbolic link %s warning: the -E option has no effect if -0 or -d is used.
 warning: the -d option is deprecated; please use -depth instead, because the latter is a POSIX-compliant feature. warning: the locate database can only be read from stdin once. warning: there is no entry in the predicate evaluation cost table for predicate %s; please report this as a bug warning: unrecognized escape `\%c' warning: unrecognized format directive `%%%c' warning: value %ld for -s option is too large, using %ld instead warning: you have specified a mode pattern %s (which is equivalent to /000). The meaning of -perm /000 has now been changed to be consistent with -perm -000; that is, while it used to match no files, it now matches all files. warning: you have specified the %s option after a non-option argument %s, but options are not positional (%s affects tests specified before it as well as those specified after it).  Please specify options before other arguments.
 write error you have too many ')' Project-Id-Version: findutils 4.5.15
Report-Msgid-Bugs-To: bug-findutils@gnu.org
POT-Creation-Date: 2015-12-27 20:34+0000
PO-Revision-Date: 2015-12-26 16:02+0100
Last-Translator: Primož Peterlin <primozz.peterlin@gmail.com>
Language-Team: Slovenian <translation-team-sl@lists.sourceforge.net>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
 
Izvajanje ukaza xargs se bo zdaj nadaljevalo, in skušal bo prebrati vhod in izvesti ukaze; če tega ne želite, vtipkajte kodo za konec datoteke.
 
GPLv3+: GNU GPL, 3. izdaja ali poznejša <http://www.gnu.org/licenses/gpl.html>
To je prosto programje; lahko ga redistribuirate in/ali spreminjate.
Za izdelek ni NOBENEGA JAMSTVA, do z zakonom dovoljene meje.

 
Napake v programu sporočite na <bug-findutils@gnu.org>.
Napake v prevodu sporočite na <translation-team-sl@lists.sourceforge.net>.
 
Poročila o napakah: %s
Napake v prevodu sporočite na <translation-team-sl@lists.sourceforge.net>.
 
dejanja: -delete -print0 -printf FORMAT -fprintf DATOTEKA FORMAT -print
      -fprint0 DATOTEKA -fprint DATOTEKA -ls -fls DATOTEKA -prune -quit
      -exec UKAZ ; -exec UKAZ {} + -ok UKAZ ;
      -execdir UKAZ ; -execdir UKAZ {} + -okdir UKAZ ;
 
privzeta pot je trenutni imenik; privzeti izraz je -print
izraz lahko sestavljajo: operatorji, izbire, testi in dejanja:
       --help                   prikaži ta navodila in zaključi
       --process-slot-var=SPREMENLJIVKA  nastavi SPREMENLJIVKO okolja v procesih
                                 naslednikih

       --show-limits            prikaži omejitve glede dolžine ukazne vrstice
       --version                različica programa
       -context KONTEKST
       -nouser -nogroup -path VZOREC -perm [-/]ZAŠČITA -regex VZOREC
      -readable -writable -executable
      -wholename VZOREC -size N[bcwkMG] -true -type [bcdpflsD] -uid N 
      -used N -user IME -xtype [bcdpfls]   -0, --null                   deli so med seboj ločeni z znakom NUL, ne s
                                 presledkom; izbira onemogoči obdelavo 
                                 narekovajev in obrnjene poševnice, kot tudi
                                 logično obdelavo EOF
   -E NIZ                       nastavi logični niz EOF, če se pojavi NIZ v
                                 vhodni vrstici, se preostanka vhoda ne upošteva
                                 (izbiri -0 in -d prevladata nad to izbiro)
   -I R                         isto kot --replace=R
   -L, --max-lines=NAJVEČ_VRSTIC  uporabi kvečjemu NAJVEČ_VRSTIC nepraznih
                                 vhodnih vrstic na posamezno ukazno vrstico
   -P, --max-procs=NAJVEČ_PROCESOV  poganjaj kvečjemu NAJVEČ_PROCESOV naenkrat
   -a, --arg-file=DATOTEKA      preberi argumente iz datoteke namesto 
                                 s standardnega vhoda
   -d, --delimiter=ZNAK         deli v vhodnem toku so ločeni z ZNAKOM namesto
                                 s presledkom; izbira onemogoči obdelavo 
                                 narekovajev in obrnjene poševnice, kot tudi
                                 logično obdelavo EOF.
   -e, --eof[=NIZ]              enakovredno izbiri -E NIZ, če je NIZ podan;
                                 sicer ni nobenega niza za konec datoteke
   -i, --replace[=R]            niz R v ZAČETNIH_ARGUMENTIH nadomesti z imeni,
                                 prebranimi s standardnega vhoda; če R ni
                                 določen, privzemi {}
   -l[NAJVEČ_VRSTIC]            podobno kot -L, vendar privzeto vzame le eno
                                 neprazno vhodno vrstico, če NAJVEČ_VRSTIC ni
                                 definirano
   -n, --max-args=NAJVEČ_ARGUMENTOV  uporabi kvečjemu NAJVEČ_ARGUMENTOV na
                                 ukazno vrstico
   -p, --interactive            s pozivom pred izvedbo ukaza
   -r, --no-run-if-empty        če ni podan noben argument, ne poženi UKAZA;
                                 brez te izbire bo UKAZ izveden najman enkrat
   -s, --max-chars=NAJVEČ_ZNAKOV  omeji dolžino ukazne vrstice na NAJVEČ_ZNAKOV
   -t, --verbose                izpiši ukaze pred njihovo izvedbo
   -x, --exit                   zaključi če je velikost presežena (glej -s)
 Domača stran %s: <%s>
 Spletna stran %s: <http://www.gnu.org/software/%s/>.
 %s je zbirka slocate z nepodprto varnostno ravnijo %d; preskočimo jo. %s je zbirka slocate.  Podpora za tovrstne zbirke je nova, mogoče so težave. %s je zbirka slocate.  Vklapljamo izbiro »-e«. %s ni ime obstoječega uporabnika %s ni ime obstoječe skupine %s ni niti ime obstoječe skupine, niti ni videti kot številčni GID, saj ima nepričakovano pripono %s %s zaključen s signalom %d %s%s argument »%s« je preobsežen %s%s se je spremenila med izvajanjem %s (stara številka enote %ld, nova številka enote %ld, vrsta datotečnega sistema %s) [ref %ld] %s: izhod s statusom 255; prekinjamo %s: neveljavno število za izbiro -%c
 %s: neveljavna izbira -- »%c«
 %s: izbira »%c%s« ne dovoljuje argumenta
 %s: izbira »%s« je dvoumna; možnosti so: %s: izbira »--%s« ne dovoljuje argumenta
 %s: izbira »--%s« zahteva argument
 %s: izbira »-W %s« ne dovoljuje argumenta
 %s: izbira »-W %s« je dvoumna
 %s: izbira »-W %s« zahteva argument
 %s: izbira zahteva argument -- »%c«
 %s: ustavljeno s signalom %d %s: zaključeno s signalom %d %s: neprepoznana izbira »%c%s«
 %s: neprepoznana izbira »--%s«
 %s: vrednost za izbiro -%c mora biti <= %ld
 %s: vrednost za izbiro -%c mora biti >= %ld
 « © -type %c ni podprt, ker ob prevajanju find za to okolje podpora za FIFO ni bila vključena -type %c ni podprt, ker ob prevajanju find za to okolje podpora za vrata Solaris ni bila vključena -type %c ni podprt, ker ob prevajanju find za to okolje podpora za poimenovane vtičnice ni bila vključena -type %c ni podprt, ker ob prevajanju find za to okolje podpora za simbolne povezave ni bila vključena < %s ... %s > ?  Vsa imena datotek: %s
 Argument k izbiri -type lahko vsebuje le en znak Standardnega vhoda ni mogoče zapreti Časa nastanka datoteke %s ni mogoče ugotoviti Vhodne datoteke %s ni mogoče odpreti Seznama priklopljenih naprav ni mogoče prebrati. Seznama priklopljenih datotečnih sistemov ni mogoče prebrati Signalnega ročnika SIGUSR1 ni mogoče nastaviti Signalnega ročnika SIGUSR2 ni mogoče nastaviti Faktor kompresije %4.2f%% (večja vrednost je boljša)
 Kompresijski faktor ni določen
 Zbirka %s je v %s obliki.
 Podatkovna zbirka je bila nazadnje posodobljena v %s.%09ld Manjkajoč argument k izbiri -D. Spremenljivka okolja %s ni nastavljena na veljavno decimalno število Eric B. Decker Izbira %s pričakuje pozitivni desetiški argument namesto %s Pričakuje se celo število: %s Celotna ovržba privilegijev ni uspela Inicializacija porazdeljene zgoščevalne tabele ni uspela Branje s standardnega izhoda ni uspelo Varna zamenjava imenika v %s ni uspela Neuspela shranitev delovnega imenika, da bi lahko pognali ukaz na %s Pisanje na izhod ni uspelo (na stopnji %d) Izpis pozivnika za -ok ni uspel Pisanje na standardni izhod ni uspelo Pisanje na standardni izhod za napake ni uspelo Omogočene možnosti:  Datotečni deskriptor %d bo puščal; prosim, javite to kot napako in podajte natančen opis najenostavnejše situacije, ki privede to te težave Skupna dolžina imen datotek je %s bajtov.
Od teh imen datotek,

	%s vsebuje presledke,
	%s vsebuje znak za novo vrstico,
	in %s vsebuje znake s postavljenim osmim bitom.
 Odkrita zanka v datotečnem sistemu: %s je del iste zanke datotečnega sistema kot %s. Odkrita zanka v datotečnem sistemu: %s ima isto številko enote in inoda kot imenik %d ravni višje. Odkrita zanka v datotečnem sistemu: %s ima isto številko enote in inoda kot imenik %d raven višje. Odkrita zanka v datotečnem sistemu: %s ima isto številko enote in inoda kot imenik %d ravni višje. Odkrita zanka v datotečnem sistemu: %s ima isto številko enote in inoda kot imenik %d ravni višje. Splošna pomoč za rabo programja GNU: <http://www.gnu.org/gethelp/>.
 Niza %s ni mogoče tolmačiti kot datum ali čas Ne upošteva se neprepoznano stikala za odpravljanje napak %s V %s se mora %s pojaviti samostojno, vi pa ste določili %s Neveljaven argument %s za izbiro --max-database-age Neveljaven argument %s za -used Neveljaven argument »%s%s« za -size Neveljavno ubežno zaporedje %s v specifikaciji vhodnega razmejilnika. Neveljavno ubežno zaporedje %s v specifikaciji vhodnega razmejilnika; vrednost znaka ne sme presegati %lo. Neveljavno ubežno zaporedje %s v specifikaciji vhodnega razmejilnika; vrednost znaka ne sme presegati %lx. Neveljavno ubežno zaporedje %s v specifikaciji vhodnega razmejilnika; neprepoznan sledilni znak %s. Neveljavno specifikacija vhodnega razmejilnika %s: razmejilnim mora biti bodisi en sam znak, bodisi ubežno zaporedje, ki se začenja z \. Neveljavna raven optimizacije %s James Youngman Kevin Dalley Velikost zbirke locate: %s bajtov
 Velikost zbirke locate: %s bajt
 Velikost zbirke locate: %s bajta
 Velikost zbirke locate: %s bajti
 Argumenti, ki so obvezni ali neobvezni pri dolgi obliki izbire,
so obvezni ali neobvezni tudi pri pripadajoči kratki obliki izbire.
 Ujemajoča se imena datotek: %s
 Zbirka locate »%s« stare oblike je prekratka, da bi lahko bila veljavna Podprta je le enkratna navedba {} v kombinaciji z -exec%s ... + Raven optimizacije %lu je previsoka.  Če želite najti datoteke zares hitro, razmislite o uporabi GNU locate. Priprava paketa: %s
 Priprava paketa: %s (%s)
 Prosimo, navedite desetiško število neposredno za -O Napake v programu %s sporočite na: %s
Napake v prevodu sporočite na <translation-team-sl@lists.sourceforge.net>.
 Napake lahko sporočate (in sledite napredek pri popravljanju) na spletni strani
za prijavljanje napak findutils, http://savannah.gnu.org/, ali, če nimate
dostopa do spleta, po elektronski pošti na naslov <bug-findutils@gnu.org>. Izvede se UKAZ z ZAČETNIMI_ARGUMENTI in dodatnimi argumenti, prebranimi z vhoda.

 Varnostna raven %s ima nepričakovano pripono %s. Varnostna raven %s je zunaj dovoljenega obsega. Nekatere datoteke so bile pri filtriranju izpuščene, zato ni mogoče izračunati faktorja kompresije.
 Simbolna povezava »%s« je del zanke v drevesu imenikov; imenik, na katerega kaže, smo že obiskali. Preizkus %s potrebuje argument Izbiri -O mora neposredno slediti desetiško celo število Dejanje -delete samodejno vklopi izbiro -depth, dejanje -prune pa ob vklopljeni izbiri -depth ne izvede ničesar. Če želite vseeno nadaljevati, izrecno uporabite izbiro -depth. Izbira -show-control-chars zahteva en sam argument, ki je bodisi »literal« bodisi »safe« Argument k izbiri --max-database-age ne sme biti prazen Argument k izbiri -user ne sme biti prazen Knjižnična funkcija atexit ni uspela Trenutni imenik je naveden v spremenljivki PATH, kar je nevarno v kombinaciji z dejanjem %s programa find. Prosim, odstranite trenutni imenik iz spremenljivke $PATH - odstranite ».«, podvojena dvopičja ter uvodna ali zaključna dvopičja. Zbirka uporablja vrstni red zlogov »little-endian«.
 Zbirka uporablja vrstni red zlogov »big-endian«.
 Vrstni red zlogov v zbirki ni razviden.
 Okolje je preobsežno za klic exec(). Spremenljivka FIND_BLOCK_SIZE ni podprta; na velikost bloka vpliva spremenljivka POSIXLY_CORRECT Relativna pot %s je vključena v spremenljivki PATH, kar je nevarno v kombinaciji z dejanjem %s programa find. Prosim, odstranite trenutni imenik iz spremenljivke $PATH Ta sistem ne omogoča ugotavljanja časa nastanka datoteke Nepričakovana pripona %s pri %s Nepoznan argument k izbiri -type: %c Neznan tip regularnega izraza %s; veljavni tipi so %s. Neznana sistemska napaka Uporaba: %s [--version | --help]
ali      %s najpogostejši_bigrami < seznam > kodiran_seznam
 Uporaba: %s [-0 | --null] [--version] [--help]
 Uporaba: %s [-H] [-L] [-P] [-Oraven] [-D Uporaba: %s [-d path | --database=pot] [-e | -E | --[non-]existing]
        [-i | --ignore-case] [-w | --wholename] [-b | --basename]
        [--limit=N | -l N] [-S | --statistics] [-0 | --null] [-c | --count]
        [-P | -H | --nofollow] [-L | --follow] [-m | --mmap] [-s | --stdio]
        [-A | --all] [-p | --print] [-r | --regex] [--regextype=TIP]
        [--max-database-age D] [--version] [--help] 
        vzorec...
 Uporaba: %s [IZBIRA]... UKAZ [ZAČETNI_ARGUMENTI]...
 Veljavni argumenti so: OPOZORILO: Sled za %lu procesi nasledniki se je izgubila OPOZORILO: na vhodu se je pojavil znak NUL.  Ni ga mogoče prenesti v seznamu argumentov.  Ste nameravali uporabiti izbiro --null? OPOZORILO: ni mogoče ugotoviti časa nastanka datoteke %s OPOZORILO: videti je, da ima datoteka %s zaščito 0000 OPOZORILO: datotečni sistem %s je bil nedavno priklopljen. OPOZORILO: datotečni sistem %s je bil nedavno odklopljen. OPOZORILO: zbirla locate %s je bila zgrajena z drugačnim vrstnim redom zlogov Opozorilo: %s se bo izvedel najmanj enkrat.  Če tega ne želite, vnesite kodo za prekinitev.
 Avtorja: %s in %s.
 Avtorji: %s, %s, %s
%s, %s, %s, %s
%s, %s in drugi.
 Avtorji: %s, %s, %s
%s, %s, %s, %s
%s in %s.
 Avtorji: %s, %s, %s
%s, %s, %s, %s
in %s.
 Avtorji: %s, %s, %s
%s, %s, %s in %s.
 Avtorji: %s, %s, %s
%s, %s in %s.
 Avtorji: %s, %s, %s
%s in %s.
 Avtorji: %s, %s, %s
in %s.
 Avtorji: %s, %s in %s.
 Avtor(ica): %s.
 Raba {} znotraj imena pomožnega programa za -execdir in -okdir zaradi mogočih varnostnih problemov ni dovoljena. Varnostno raven morate navesti kot desetiško celo število. Navedli ste izbiro -E, vendar te izbire ni mogoče uporabiti z zbirkami oblike slocate, ki imajo varnostno raven, višjo od 0.  Za to zbirko ne bo ustvarjenih rezultatov.
 ] [POT...] [IZRAZ]
 ^[Nn] ^[DdJj] » dvoumen argument %s za %s vrstica z argumenti je predolga seznam argumentov je predolg argument k izbiri -group manjka; navedeno bi moralo biti ime skupine  aritmetična prekoračitev obsega pri poskusu izračuna konca trenutnega datuma  aritmetična prekoračitev obsega pri pretvorbi %s dni v število sekund klic exec() ni mogoč zaradi omejitev glede velikosti argumenta ni mogoče izbrisati %s posameznega argumenta ni mogoče umestiti v obseg seznama argumentov vejitev ni mogoča ni mogoče preiskati %s statusa trenutnega imenika ni mogoče ugotoviti ukaz je predolg ustvarjanje cevovoda pred vejitvijo ni uspelo dni dvojni okolje je preobsežno za klic exec varno branje medpomnilnika errno v xargs_do_exec ni uspelo (to je najverjetneje napaka; prosimo, javite jo) napaka pri zapiranju datoteke napaka pri branju besede z %s napaka pri čakanju na %s napaka pri čakanju na proces naslednik napaka: %s na koncu oblikovnega niza napaka: formatno določilo »%%%c« je rezervirano za rabo v prihodnosti pričakuje se izraz, ki sledi »%s« pričakuje se izraz med »%s« in »)« ovržba privilegijev skupine ni uspela ovržba privilegijev setgid ni uspela ovržba privilegijev setuid ni uspela odpiranje /dev/tty za branje ni uspelo povrnitev delovnega imenika po preiskovanju %s ni mogoča vrnitev v začetni delovni imenik ni uspela vrnitev v nadrejeni imenik ni mogoča nastavitev spremenljivke okolja %s ni uspela ni mogoče odnastaviti spremenljivke %s klic getfilecon ni uspel: %s neveljaven argument %s%s za »%s« neveljaven tip -size: »%c« neveljaven argument %s za %s neveljaven argument »%s« za »%s« neveljaven izraz neveljaven izraz: manjkajoč zaklepaj »)«. neveljaven izraz; prazni oklepaji niso dovoljeni neveljaven izraz: pričakovali smo zaklepaj »)«, ki ga ni.  Morda potrebujete dodatni predikat za »%s« neveljaven izraz; preveč zaklepajev »)« neveljaven izraz: uporabili ste binarni operator  »%s«, pred katerim ni argumenta neveljaven način %s neveljaven prazni argument pri -size neveljaven predikat -context: SELinux ni omogočen neveljaven predikat »%s« neveljavna pripona argumenta %s%s za »%s« zbirka locate %s vsebuje ime datoteke, ki je daljše, kot ga lahko locate obdela zbirka locate %s je poškodovana ali neveljavna zbirka locate %s je videti kot zbirka slocate, vendar je videti, da ima nastavljeno varnostno raven %c, ki je GNU findutils za zdaj ne podpira pomnilnik porabljen manjkajoč argument k »%s« opla - neveljavno privzeto vstavljanje logičnega ALI! opla -- neveljaven tip izraza (%d)! opla -- neveljaven tip izraza! operatorji (po padajoči prednosti; če ni podano nič drugega, se privzame -and):
      ( IZRAZ )   ! IZRAZ   -not IZRAZ   IZRAZ1 -a IZRAZ2   IZRAZ1 -and -IZRAZ2
      IZRAZ1 -o IZRAZ2   IZRAZ1 -or IZRAZ2   IZRAZ1 , IZRAZ2
 izbire --%s ni dovoljeno nastaviti na vrednost, ki vsebuje »=« poti morajo biti navedene pred izrazom: %s pozicijske izbire (vedno resnične): -daystart -follow -regextype

navadne izbire (vedno resnične, navedene pred drugimi izrazi):  
      -depth  --help -maxdepth RAVNI -mindepth RAVNI -mount -noleaf 
      --version -xdev -ignore_readdir_race -noignore_readdir_race
testi (N can be +N or -N or N): -amin N -anewer DATOTEKA -atime N -cmin N
 branje je vrnilo nepričakovano vrednost %zu; to je najverjetneje napaka, prosimo, javite jo preizkus koherentnosti knjižnične funkcije fnmatch() ni uspel. enojni raven varnosti %ld pri slocate ni podprta. standardni izhod za napake standardni izhod testi (N je lakko +N, -N ali N): -amin N -anewer FILE -atime N -cmin N
      -cnewer DATOTEKA -ctime N -empty -false -fstype TIP -gid N -group IME
      -ilname VZOREC -iname VZOREC -inum N -iwholename VZOREC -iregex VZOREC
      -links N -lname VZOREC -mmin N -mtime N -name VZOREC -newer DATOTEKA sistemski klic time() ni uspel pomnilnika ni mogoče dodeliti začetnega delovnega imenika ni uspelo zabeležiti nepričakovan EOF v %s nepričakovan dodatni predikat nepričakovan dodatni predikat »%s« neznano neznan predikat »%s« %s narekovaj brez para; privzeto so narekovaji za xargs posebni, razen če vključite izbiro -O opozorilo: -%s %s se zaključi z /., zato se z njim ne bo ujemalo nič opozorilo: Imena datotek v Unixu navadno ne vsebujejo poševnic (poti pa jih). To pomeni, da se bo »%s %s« na tem sistemu najverjetneje vedno ovrednotilo kot neresnično. Testa »-wholename« ali »-samefile« bosta morda uporabnejša. Če uporabljate GNU grep, lahko uporabite tudi »find ... -print0 | grep -FzZ %s«. opozorilo: zbirka »%s« je starejša od %d %s (dejanska starost je %.1f %s) opozorilo: ubežnica »\«, ki ji ne sledi nič opozorilo: formatnemu določilu »%%%c« bi moral slediti drug znak opozorilo: simbolni povezavi %s ne sledimo opozorilo: izbira -E nima vpliva, če sta uporabljena -0 ali -d.
 opozorilo: raba izbire -d je odsvetovana; zaradi skladnosti s POSIX namesto nje priporočamo -depth. opozorilo: zbirko locate lahko preberemo s standardnega vhoda le enkrat. opozorilo: v stroškovni tabeli za vrednotenje predikatov ni vnosa za predikat %s; prosimo, javite to kot napako opozorilo: neprepoznano ubežno zaporedje »\%c« opozorilo: neprepoznano formatno določilo »%%%c« opozorilo: vrednost %ld za izbiro -s je previsoka, namesto nje uporabljamo %ld opozorilo: določili ste vzorec %s, ki je enakovreden /000. Pomen -perm /000 se je spremenil, tako da je skladen z -perm -000; prej se ni ujemal z nobeno datoteke, zdaj pa se z vsemi. opozorilo: izbiro %s ste navedli za neizbirnim argumentom %s, vendar izbire niso pozicijske (%s vpliva na teste, navadene pred njo in za njo). Prosimo, navedite izbire pred drugimi argumenti.
 napaka pri pisanju preveč zaklepajev »)« PRIuMAX %s%s changed during execution of %s (old inode number %, new inode number %, file system type is %s) [ref %ld] WARNING: Hard link count is wrong for %s (saw only st_nlink=% but we already saw % subdirectories): this may be a bug in your file system driver.  Automatically turning on find's -noleaf option.  Earlier results may have failed to include directories that should have been searched. Your environment variables take up % bytes
 POSIX upper limit on argument length (this system): %
 POSIX smallest allowable upper limit on argument length (all systems): %
 Maximum length of command we could actually use: %
 Size of command buffer we are actually using: %
 Maximum parallelism (--max-procs must be no greater): %
 %s%s se je spremenila med izvajanjem %s (stara številka inoda %, nova številka inoda %, vrsta datotečnega sistema %s) [ref %ld] OPOZORILO: Število trdih povezav za %s je napačno (vidno je st_nlink=%, vendar smo našteli že % podimenikov): lahko gre za napako v gonilniku za vaš datotečni sistem. Samodejno vklapljamo izbiro -noleaf. Prejšnji rezultati so lahko bili napačni, ker v iskanje niso bili vključeni vsi potrebni imeniki. Vaše spremenljivke okolja zasedajo % bajtov
 Zgornja meja POSIX za dolžino argumenta na tem sistemu: %
 Najmanjša dovoljena zgornja meja POSIX za dolžino argumenta na kateremkoli sistemu: %
 Največja dolžina ukaza, ki ga lahko uporabimo: %
 Velikost ukaznega medpomnilnika, ki ga dejansko uporabljamo: %
 Maksimiraj hkratno izvajanje (--max-procs ne sme biti večji): %
 