��     0   �  {  @     ,     4  T      �  �   �  �   �  )   M     w  �   �  �   �  :     O   J  B   �  C   �     !  �   9  �     �   �  3   �  }   �  I   z   L   �   �   !  �   �!  �   �"  �   P#  O   �#  >   ?$  �   ~$  I   S%  D   �%  E   �%     (&  0   <&  H   m&  N   �&  7   '  "   ='  '   `'  |   �'     (      (  t   =(  $   �(  "   �(     �(  ,   )  ,   C)  ,   p)  '   �)  -   �)      �)  (   *  (   =*     f*     *     �*     �*  *   �*  *   +     1+     3+  _   7+  g   �+  g   �+  h   g,     �,     �,  1   �,     &-  #   B-     f-  $   �-  $   �-  !   �-  !   �-  -   .     <.  !   \.  &   ~.      �.  <   �.     /  >   /     Q/     i/  +   �/     �/  )   �/  @   �/  $   :0     _0  "   ~0     �0     �0  �   �0  �   f1  I   2    d2  ?   s3  9   �3  #   �3  8   4  1   J4     |4      �4  <   �4  b   �4  b   Z5  c   �5  �   !6     �6     �6     �6  >   �6  t   7     �7  6   �7  7   �7  g   8     8     �8  4   �8     �8  �   �8  M   �9  +   �9  3   ":  W   V:  x   �:     ';  ?   E;  �   �;  X   1<  <   �<  )   �<  "   �<  �   =  3   >  6   C>  9   z>  (   �>  �   �>  �   o?  D   @     Y@     t@  7   �@     �@  [   �@  -   ;A  '   iA  �  �A  0   1C     bC  *   wC  �   �C  /   -D  *   ]D  2   �D  4   �D  A   �D  o   2E     �E  ;   �E  3   �E  /   )F  +   YF  '   �F  #   �F     �F     �F     G  q   G  :   �G  �   �G     vH     �H     �H     �H     �H     �H     �H  7   �H  =   !I  C   _I  3   �I     �I  :   �I     #J     /J     @J     ^J  !   oJ     �J     �J  !   �J  Y   �J     K     ,K     IK     ^K  !   ~K  =   �K  !   �K  +    L     ,L      LL      mL  #   �L  6   �L  -   �L  $   M  %   <M  '   bM     �M     �M     �M     �M     �M     N  P   N  6   pN  n   �N  )   O  P   @O     �O     �O  3   �O     �O  $   P  D   0P  (   uP  �   �P     'Q     8Q  )   QQ  %   {Q      �Q  �   �Q  8   �R  !   �R  	  �R  L   �S  6   CT     zT  *   �T     �T     �T  )  �T     �U     V  *   'V     RV     gV     �V     �V     �V  W   �V  ?   W  I  YW  C   �X  .   �X  H   Y  +   _Y  :   �Y  q   �Y  >   8Z  o   wZ  "   �Z  -   
[  @   8[  �   y[  �   [\     A]     M]  �  c]  �   N_  �   �_  4   �`     a  �   'a  �   &b  B   �b  H   �b  H   6c  B   c     �c  �   �c  �   �d    �e  9   �f  �   �f  X   �g  Z   �g    =h  �   ?i  �   �i  �   yj  b   k  I   ok  �   �k  \   �l  C   �l  P   3m     �m  3   �m  P   �m  y    n  3   �n     �n     �n  o    o     po  #   �o  v   �o  &   %p  "   Lp     op  5   �p  6   �p  5   �p  4   0q  6   eq  "   �q  5   �q  +   �q     !r     :r     Ur     ur  3   �r  *   �r     �r     �r  ^   �r  e   Ys  f   �s  g   &t     �t     �t  7   �t  %   �t  ,   u  "   <u  /   _u  3   �u  ,   �u  ,   �u  0   v  $   Nv     sv  "   �v     �v  =   �v     w  B   #w     fw  -   |w  ;   �w  )   �w  5   x  C   Fx  (   �x  .   �x  (   �x  +   y     7y  �   Ky  �   �y  C   �z  �   �z  J   �{  6   |  (   O|  0   x|  8   �|  '   �|  )   
}  P   4}  u   �}  u   �}  r   q~  �   �~  %   o     �     �  Q   �  �   �     ��  H   ��  6   �  p   �     ��     ��  %   ��  #   Ձ  �   ��  Q   ̂  4   �  6   S�  U   ��  f   ��  )   G�  5   q�  �   ��  T   ��  ?   ܅  ,   �  $   I�  �   n�  2   Y�  5   ��  >     !   �  �   #�  �   ��  O   >�     ��  '   ��  9   ԉ     �  \    �  ,   }�  &   ��  �  ъ  )   p�      ��  0   ��  x   �  6   e�  -   ��  +   ʍ  -   ��  G   $�  i   l�     ֎  8   �  1   %�  .   W�  )   ��  %   ��  !   ֏     ��     �     1�  �   A�  6     �   ��     ��     ��     ��     ȑ  "   ˑ     �     �  C   "�  Q   f�  X   ��  M   �     _�  L   x�     œ     �  3   �     8�  0   O�     ��     ��  )   ��  �   ��     C�     [�     y�  !   ��  !   ��  ?   ܕ  !   �  +   >�  '   j�  (   ��  (   ��      �  E   �  7   K�  ,   ��  .   ��  )   ߗ     	�  ,   "�     O�      o�  (   ��     ��  1   ɘ  .   ��  l   *�  -   ��  D   ř     
�  #   !�  ?   E�     ��  ;   ��  T   ߚ  ,   4�  �   a�     ��     	�      %�     F�     a�  �   w�  5   O�  #   ��    ��  v   ��  4   .�     c�  *   i�     ��     ��  1  ��     �     �  +   �     J�     ^�  &   x�     ��     ��  f   á  N   *�  B  y�  G   ��  2   �  F   7�  +   ~�  P   ��  T   ��  I   P�  �   ��  ,   !�  .   N�  H   }�  �   Ʀ  �   ��  
   ��     ��     �          H   �   �          �   �   s   {   +   �   �   R       �         B      �   �   �   #           �   r   �   j       
  �   h   6   u   :   �        �       e   �   Z   �          @   v         }   c   �                       \   �   �   |                  p   M   �   �   �   A           �   �   �   <   �   �   �   C   "   3   �      �   �             w   ?          �       �   I     1       �       �   !       �   �              [   �   �   f       �   9   �   x   U      X           (   P              �   
       )   W           �       �   k   ^   �   �          �   �         D         �       �   �         .         �   �      �       4   *   �   �       �   �       y      o   =   �   ~       �   J      /       �   �     -   >       �   	       �                     i   F   �   �   �   K   �   �   �   n                   _   �       �      �          �   N   �     G          �   �   �   �   �               8       �       �   �   �   Q       5           �           	          &   %   �       t   �   �       �   �       �   �       �       Y       a   g   �   `   �       �                �         �   �   �   �   E      �   �                   q   �          ]   �   S                 �   7   �   �         �   �           �              �   T           �   �       �   �     �   z           O   2   L       �   V   �   �   m   �   d           �     l   ;   0   $   �   �       '       �   b          �              ,      ̨  t  �  �  �  �  �  �    $  @  \  p  �  �  �  �  Ԩ  7              $   ����C�  =              �   ����^�  $          ������  5          ������  H          �����  2          ����?�  /          ����p�  7          ������  =                  �����  D              �   ����M�            ����y�  =          ������  C          ������  6          ����5�  9          ����p�  J          ���� 
Execution of xargs will continue now, and it will try to read its input and run commands; if this is not what you wanted to happen, please type the end-of-file keystroke.
 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to <bug-findutils@gnu.org>.
 
Report bugs to: %s
 
actions: -delete -print0 -printf FORMAT -fprintf FILE FORMAT -print 
      -fprint0 FILE -fprint FILE -ls -fls FILE -prune -quit
      -exec COMMAND ; -exec COMMAND {} + -ok COMMAND ;
      -execdir COMMAND ; -execdir COMMAND {} + -okdir COMMAND ;
 
default path is the current directory; default expression is -print
expression may consist of: operators, options, tests, and actions:
       --help                   display this help and exit
       --process-slot-var=VAR   set environment variable VAR in child processes
       --show-limits            show limits on command-line length
       --version                output version information and exit
       -context CONTEXT
       -nouser -nogroup -path PATTERN -perm [-/]MODE -regex PATTERN
      -readable -writable -executable
      -wholename PATTERN -size N[bcwkMG] -true -type [bcdpflsD] -uid N
      -used N -user NAME -xtype [bcdpfls]   -0, --null                   items are separated by a null, not whitespace;
                                 disables quote and backslash processing and
                                 logical EOF processing
   -E END                       set logical EOF string; if END occurs as a line
                                 of input, the rest of the input is ignored
                                 (ignored if -0 or -d was specified)
   -I R                         same as --replace=R
   -L, --max-lines=MAX-LINES    use at most MAX-LINES non-blank input lines per
                                 command line
   -P, --max-procs=MAX-PROCS    run at most MAX-PROCS processes at a time
   -a, --arg-file=FILE          read arguments from FILE, not standard input
   -d, --delimiter=CHARACTER    items in input stream are separated by CHARACTER,
                                 not by whitespace; disables quote and backslash
                                 processing and logical EOF processing
   -e, --eof[=END]              equivalent to -E END if END is specified;
                                 otherwise, there is no end-of-file string
   -i, --replace[=R]            replace R in INITIAL-ARGS with names read
                                 from standard input; if R is unspecified,
                                 assume {}
   -l[MAX-LINES]                similar to -L but defaults to at most one non-
                                 blank input line if MAX-LINES is not specified
   -n, --max-args=MAX-ARGS      use at most MAX-ARGS arguments per command line
   -p, --interactive            prompt before running commands
   -r, --no-run-if-empty        if there are no arguments, then do not run COMMAND;
                                 if this option is not given, COMMAND will be
                                 run at least once
   -s, --max-chars=MAX-CHARS    limit length of command line to MAX-CHARS
   -t, --verbose                print commands before executing them
   -x, --exit                   exit if the size (see -s) is exceeded
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s is an slocate database of unsupported security level %d; skipping it. %s is an slocate database.  Support for these is new, expect problems for now. %s is an slocate database.  Turning on the '-e' option. %s is not the name of a known user %s is not the name of an existing group %s is not the name of an existing group and it does not look like a numeric group ID because it has the unexpected suffix %s %s terminated by signal %d %s%s argument '%s' too large %s%s changed during execution of %s (old device number %ld, new device number %ld, file system type is %s) [ref %ld] %s: exited with status 255; aborting %s: invalid number for -%c option
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: stopped by signal %d %s: terminated by signal %d %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 %s: value for -%c option should be <= %ld
 %s: value for -%c option should be >= %ld
 ' (C) -type %c is not supported because FIFOs are not supported on the platform find was compiled on. -type %c is not supported because Solaris doors are not supported on the platform find was compiled on. -type %c is not supported because named sockets are not supported on the platform find was compiled on. -type %c is not supported because symbolic links are not supported on the platform find was compiled on. < %s ... %s > ?  All Filenames: %s
 Arguments to -type should contain only one letter Cannot close standard input Cannot obtain birth time of file %s Cannot open input file %s Cannot read list of mounted devices. Cannot read mounted file system list Cannot set SIGUSR1 signal handler Cannot set SIGUSR2 signal handler Compression ratio %4.2f%% (higher is better)
 Compression ratio is undefined
 Database %s is in the %s format.
 Database was last modified at %s.%09ld Empty argument to the -D option. Environment variable %s is not set to a valid decimal number Eric B. Decker Expected a positive decimal integer argument to %s, but got %s Expected an integer: %s Failed to fully drop privileges Failed to initialize shared-file hash table Failed to read from stdin Failed to safely change directory into %s Failed to save working directory in order to run a command on %s Failed to write output (at stage %d) Failed to write prompt for -ok Failed to write to standard output Failed to write to stderr Features enabled:  File descriptor %d will leak; please report this as a bug, remembering to include a detailed description of the simplest way to reproduce this problem. File names have a cumulative length of %s bytes.
Of those file names,

	%s contain whitespace, 
	%s contain newline characters, 
	and %s contain characters with the high bit set.
 File system loop detected; %s is part of the same file system loop as %s. Filesystem loop detected; %s has the same device number and inode as a directory which is %d level higher in the file system hierarchy Filesystem loop detected; %s has the same device number and inode as a directory which is %d levels higher in the file system hierarchy General help using GNU software: <http://www.gnu.org/gethelp/>
 I cannot figure out how to interpret %s as a date or time Ignoring unrecognised debug flag %s In %s the %s must appear by itself, but you specified %s Invalid argument %s for option --max-database-age Invalid argument %s to -used Invalid argument `%s%s' to -size Invalid escape sequence %s in input delimiter specification. Invalid escape sequence %s in input delimiter specification; character values must not exceed %lo. Invalid escape sequence %s in input delimiter specification; character values must not exceed %lx. Invalid escape sequence %s in input delimiter specification; trailing characters %s not recognised. Invalid input delimiter specification %s: the delimiter must be either a single character or an escape sequence starting with \. Invalid optimisation level %s James Youngman Kevin Dalley Locate database size: %s byte
 Locate database size: %s bytes
 Mandatory and optional arguments to long options are also
mandatory or optional for the corresponding short option.
 Matching Filenames: %s
 Old-format locate database %s is too short to be valid Only one instance of {} is supported with -exec%s ... + Optimisation level %lu is too high.  If you want to find files very quickly, consider using GNU locate. Packaged by %s
 Packaged by %s (%s)
 Please specify a decimal number immediately after -O Report %s bugs to: %s
 Report (and track progress on fixing) bugs via the findutils bug-reporting
page at http://savannah.gnu.org/ or, if you have no web access, by sending
email to <bug-findutils@gnu.org>. Run COMMAND with arguments INITIAL-ARGS and more arguments read from input.

 Security level %s has unexpected suffix %s. Security level %s is outside the convertible range. Some filenames may have been filtered out, so we cannot compute the compression ratio.
 Symbolic link %s is part of a loop in the directory hierarchy; we have already visited the directory to which it points. The %s test needs an argument The -O option must be immediately followed by a decimal integer The -delete action automatically turns on -depth, but -prune does nothing when -depth is in effect.  If you want to carry on anyway, just explicitly use the -depth option. The -show-control-chars option takes a single argument which must be 'literal' or 'safe' The argument for option --max-database-age must not be empty The argument to -user should not be empty The atexit library function failed The current directory is included in the PATH environment variable, which is insecure in combination with the %s action of find.  Please remove the current directory from your $PATH (that is, remove ".", doubled colons, or leading or trailing colons) The database has big-endian machine-word encoding.
 The database has little-endian machine-word encoding.
 The database machine-word encoding order is not obvious.
 The environment is too large for exec(). The environment variable FIND_BLOCK_SIZE is not supported, the only thing that affects the block size is the POSIXLY_CORRECT environment variable The relative path %s is included in the PATH environment variable, which is insecure in combination with the %s action of find.  Please remove that entry from $PATH This system does not provide a way to find the birth time of a file. Unexpected suffix %s on %s Unknown argument to -type: %c Unknown regular expression type %s; valid types are %s. Unknown system error Usage: %s [--version | --help]
or     %s most_common_bigrams < file-list > locate-database
 Usage: %s [-0 | --null] [--version] [--help]
 Usage: %s [-H] [-L] [-P] [-Olevel] [-D  Usage: %s [-d path | --database=path] [-e | -E | --[non-]existing]
      [-i | --ignore-case] [-w | --wholename] [-b | --basename] 
      [--limit=N | -l N] [-S | --statistics] [-0 | --null] [-c | --count]
      [-P | -H | --nofollow] [-L | --follow] [-m | --mmap] [-s | --stdio]
      [-A | --all] [-p | --print] [-r | --regex] [--regextype=TYPE]
      [--max-database-age D] [--version] [--help]
      pattern...
 Usage: %s [OPTION]... COMMAND [INITIAL-ARGS]...
 Valid arguments are: WARNING: Lost track of %lu child processes WARNING: a NUL character occurred in the input.  It cannot be passed through in the argument list.  Did you mean to use the --null option? WARNING: cannot determine birth time of file %s WARNING: file %s appears to have mode 0000 WARNING: file system %s has recently been mounted. WARNING: file system %s has recently been unmounted. WARNING: locate database %s was built with a different byte order Warning: %s will be run at least once.  If you do not want that to happen, then press the interrupt keystroke.
 Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 You may not use {} within the utility name for -execdir and -okdir, because this is a potential security problem. You need to specify a security level as a decimal integer. You specified the -E option, but that option cannot be used with slocate-format databases with a non-zero security level.  No results will be generated for this database.
 ] [path...] [expression]
 ^[nN] ^[yY] ` ambiguous argument %s for %s argument line too long argument list too long argument to -group is empty, but should be a group name arithmetic overflow when trying to calculate the end of today arithmetic overflow while converting %s days to a number of seconds can't call exec() due to argument size restrictions cannot delete %s cannot fit single argument within argument list size limit cannot fork cannot search %s cannot stat current directory command too long could not create pipe before fork days double environment is too large for exec errno-buffer safe_read failed in xargs_do_exec (this is probably a bug, please report it) error closing file error reading a word from %s error waiting for %s error waiting for child process error: %s at end of format string error: the format directive `%%%c' is reserved for future use expected an expression after '%s' expected an expression between '%s' and ')' failed to drop group privileges failed to drop setgid privileges failed to drop setuid privileges failed to open /dev/tty for reading failed to restore working directory after searching %s failed to return to initial working directory failed to return to parent directory failed to set environment variable %s failed to unset environment variable %s getfilecon failed: %s invalid %s%s argument '%s' invalid -size type `%c' invalid argument %s for %s invalid argument `%s' to `%s' invalid expression invalid expression; I was expecting to find a ')' somewhere but did not see one. invalid expression; empty parentheses are not allowed. invalid expression; expected to find a ')' but didn't see one.  Perhaps you need an extra predicate after '%s' invalid expression; you have too many ')' invalid expression; you have used a binary operator '%s' with nothing before it. invalid mode %s invalid null argument to -size invalid predicate -context: SELinux is not enabled. invalid predicate `%s' invalid suffix in %s%s argument '%s' locate database %s contains a filename longer than locate can handle locate database %s is corrupt or invalid locate database %s looks like an slocate database but it seems to have security level %c, which GNU findutils does not currently support memory exhausted missing argument to `%s' oops -- invalid default insertion of and! oops -- invalid expression type (%d)! oops -- invalid expression type! operators (decreasing precedence; -and is implicit where no others are given):
      ( EXPR )   ! EXPR   -not EXPR   EXPR1 -a EXPR2   EXPR1 -and EXPR2
      EXPR1 -o EXPR2   EXPR1 -or EXPR2   EXPR1 , EXPR2
 option --%s may not be set to a value which includes `=' paths must precede expression: %s positional options (always true): -daystart -follow -regextype

normal options (always true, specified before other expressions):
      -depth --help -maxdepth LEVELS -mindepth LEVELS -mount -noleaf
      --version -xdev -ignore_readdir_race -noignore_readdir_race
 read returned unexpected value %zu; this is probably a bug, please report it sanity check of the fnmatch() library function failed. single slocate security level %ld is unsupported. standard error standard output tests (N can be +N or -N or N): -amin N -anewer FILE -atime N -cmin N
      -cnewer FILE -ctime N -empty -false -fstype TYPE -gid N -group NAME
      -ilname PATTERN -iname PATTERN -inum N -iwholename PATTERN -iregex PATTERN
      -links N -lname PATTERN -mmin N -mtime N -name PATTERN -newer FILE time system call failed unable to allocate memory unable to record current working directory unexpected EOF in %s unexpected extra predicate unexpected extra predicate '%s' unknown unknown predicate `%s' unmatched %s quote; by default quotes are special to xargs unless you use the -0 option warning: -%s %s will not match anything because it ends with /. warning: Unix filenames usually don't contain slashes (though pathnames do).  That means that '%s %s' will probably evaluate to false all the time on this system.  You might find the '-wholename' test more useful, or perhaps '-samefile'.  Alternatively, if you are using GNU grep, you could use 'find ... -print0 | grep -FzZ %s'. warning: database %s is more than %d %s old (actual age is %.1f %s) warning: escape `\' followed by nothing at all warning: format directive `%%%c' should be followed by another character warning: not following the symbolic link %s warning: the -E option has no effect if -0 or -d is used.
 warning: the -d option is deprecated; please use -depth instead, because the latter is a POSIX-compliant feature. warning: the locate database can only be read from stdin once. warning: there is no entry in the predicate evaluation cost table for predicate %s; please report this as a bug warning: unrecognized escape `\%c' warning: unrecognized format directive `%%%c' warning: value %ld for -s option is too large, using %ld instead warning: you have specified a mode pattern %s (which is equivalent to /000). The meaning of -perm /000 has now been changed to be consistent with -perm -000; that is, while it used to match no files, it now matches all files. warning: you have specified the %s option after a non-option argument %s, but options are not positional (%s affects tests specified before it as well as those specified after it).  Please specify options before other arguments.
 write error you have too many ')' Project-Id-Version: findutils-4.5.15
Report-Msgid-Bugs-To: bug-findutils@gnu.org
POT-Creation-Date: 2015-12-27 20:34+0000
PO-Revision-Date: 2015-12-19 10:45+0100
Last-Translator: Åka Sikrom <a4@hush.com>
Language-Team: Norwegian Bokmaal <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.5.4
X-Poedit-Bookmarks: -1,-1,242,-1,-1,-1,-1,-1,-1,-1
 
xargs er klar til å fortsette. Det vil prøve å lese inndata og kjøre kommandoer. Trykk slutt-på-fil-tastetrykket hvis du ikke vil at dette skal skje.
 
Lisens GPLv3+: GNU GPL version 3 eller nyere <http://gnu.org/licenses/gpl.html>.
Dette er fri programvare. Du står fritt til å endre og dele den videre.
Det stilles INGEN GARANTI, i den grad det tillates av gjeldende lovverk.

 
Rapporter programfeil til <bug-findutils@gnu.org>.
 
Rapporter programfeil til: %s
 
handlinger: -delete -print0 -printf FORMAT -fprintf FILFORMAT -print 
      -fprint0 FIL -fprint FIL -ls -fls FIL -prune -quit
      -exec KOMMANDO ; -exec KOMMANDO {} + -ok KOMMANDO ;
      -execdir  KOMMANDO ; -execdir KOMMANDO {} + -okdir KOMMANDO ;
 
Gjeldende mappe er standard filsti. Standarduttrykk er «-print».
Uttrykk kan bestå av operatører, valg, tester og handlinger:
       --help                   vis denne hjelpeteksten og avslutt
       --process-slot-var=VAR   bruk valgt miljøVARiabel i underprosess
       --show-limits            vis grenser for lengde på kommandolinje
       --version                vis versjonsinformasjon og avslutt
       -context KONTEKST
       -nouser -nogroup -path MØNSTER -perm [-/]MODUS -regex MØNSTER
      -readable -writable -executable
      -wholename MØNSTER -size N[bcwkMG] -true -type [bcdpflsD] -uid N
      -used N -user NAVN -xtype [bcdpfls]   -0, --null                   elementer holdes adskilt med nulltegn, ikke mellomrom.
                                 (dette slår av behandling av sitat- og skråstrektegn,
                                 samt logisk EOF-behandling)
   -E SLUTT                       velg logisk streng for hvordan filer SLUTTer. Hvis SLUTT er en linje
                                 med inndata, ignoreres evt. etterfølgende inndata
                                 (dette valget ignoreres hvis du bruker «-0» eller «-d»)
   -I R                         tilsvarer «--replace=R»
   -L, --max-lines=MAKS-ANT    ikke bruk flere enn valgt MAKS-ANTall ikke-tomme inndatalinjer
                                 per kommandolinje
   -P, --max-procs=MAKS-ANT    ikke kjør flere enn valgt MAKS-ANTall prosesser samtidig
   -a, --arg-file=FIL          les argumenter fra valgt FIL, i stedet for standard inndata
   -d, --delimiter=TEGN    elementer i inndata-strøm holdes adskilt med valgt TEGN,
                                 i stedet for mellomrom (dette slår av behandling av
                                 sitat- og skråstrektegn, samt logisk EOF-behandling)
   -e, --eof[=SLUTT]              tilsvarer «-E SLUTT» hvis SLUTT er valgt.
                                 Ellers brukes ingen slutt på fil-streng
   -i, --replace[=R]            erstatt R i ARGUMenter med navn som leses
                                 fra standard inndata. Hvis R ikke angis, brukes «{}»
   -l[MAKS-ANT]                likner «-L», men bruker maks én ikke-tom
                                 inndatalinje hvis MAKS-ANT ikke velges
   -n, --max-args=MAKS-ANT      ikke bruk flere enn valgt MAKS-ANTall argumenter per kommandolinje
   -p, --interactive            be om bekreftelse før kommandoer kjøres
   -r, --no-run-if-empty        ikke kjør KOMMANDO hvis ingen argumenter er gitt.
                                 Valgt KOMMANDO kjøres minst én gang hvis dette
                                 valget ikke brukes
   -s, --max-chars=MAKS-ANT    ikke bruk flere enn valgt MAKS-ANTall tegn på kommandolinjer
   -t, --verbose                skriv ut kommandoer før de kjøres
   -x, --exit                   avslutt hvis størrelsen (se «-s») overskrides
 Nettside for %s: <%s>
 Nettside for %s: <http://www.gnu.org/software/%s/>
 %s er en slocate-database med ustøttet sikkerhetsnivå %d, og blir hoppet over. %s er en slocate-database. Støtte for slike databaser er nettopp implementert, og de kan by på problemer inntil videre. %s er en slocate-database. Slår på valget «-e». %s er en ukjent bruker %s er et ukjent gruppenavn %s er et ukjent gruppenavn. Det ser heller ikke ut som en numerisk gruppe-ID, fordi det inneholder suffikset %s %s avsluttet av signal %d %s%s-argumentet «%s» er for stort %s%s ble endret under kjøring av %s (gammelt enhetsnummer: %ld. Nytt enhetsnummer: %ld. Filsystem-type: %s) [ref %ld] %s: avsluttet med status 255. Avbryter %s: ugyldig antall for valget -%c
 %s: «%c» er et ukjent valg
 %s: valget «%c%s» tillater ikke bruk av argumenter
 %s: valget «%s» er flertydig, og kan bety følgende: %s: valget «--%s» tillater ikke bruk av argumenter
 %s: valget «--%s» krever at du bruker et argument
 %s: valget «-W %s» tillater ikke bruk av argumenter
 %s: valget «-W %s» er flertydig
 %s: valget «-W %s» krever at du bruker et argument
 %s: «%c» krever at du bruker et argument
 %s: avbrutt av signal %d %s: avsluttet av signal %d %s: «%c%s» er et ukjent valg
 %s: «--%s» er et ukjent valg
 %s: verdien av valget -%c må være lavere enn %ld
 %s: verdi for valget -%c må være >= %ld
 » © -type «%c» støttes ikke fordi FIFO-er ikke støttes på plattformen find ble kompilert på. -type «%c» støttes ikke fordi Solaris-dører ikke støttes på plattformen find ble kompilert på. -type «%c» støttes ikke fordi sokler med navn ikke støttes på plattformen find ble kompilert på. -type «%c» støttes ikke fordi symbolske lenker ikke støttes på plattformen find ble kompilert på. < %s … %s > ?  Alle filnavn: %s
 Argumenter for «-type» kan bare inneholde én bokstav Klarte ikke å lukke standard inndata Klarte ikke å hente fødselstid for fila %s Klarte ikke å åpne inndatafil %s Klarte ikke å lese liste over monterte enheter Klarte ikke å lese liste over monterte filsystemer Klarte ikke å velge SIGUSR1-signalbehandler Klarte ikke å velge SIGUSR2-signalbehandler Komprimeringsforhold %4.2f%% (høyere er bedre)
 Komprimeringsforhold er ikke angitt
 Databasen %s er i formatet %s.
 Databasen ble sist endret %s.%09ld Valget «-D» mangler argument. Verdien av miljøvariabelen %s er ikke et ugyldig desimaltall Eric B. Decker Forventet et positivt desimal-heltallsargument for %s, men fikk %s Forventet heltall: %s Klarte ikke å fjerne rettigheter fullstendig Klarte ikke å laste inn kontrollsum-tabell for delte filer Klarte ikke å lese fra standard innkanal Klarte ikke å bytte mappe til %s på en sikker måte Klarte ikke å lagre arbeidsmappe for å kjører en kommando på %s Klarte ikke å skrive utdata ved steg %d Klarte ikke å skrive ut ledetekst for «-ok» Klarte ikke å skrive til standardutdata Klarte ikke å skrive til standardfeilkanal Funksjoner i bruk:  Fildeskriptor %d lekker. Vi ber om at du rapporterer dette som en programfeil, og tar med en detaljert beskrivelse av hvordan problemet kan gjenskapes. Filnavn har en kumulativ lengde på %s byte.
Av disse filene er det

	%s som inneholder tomrom, 
	%s som inneholder linjeskift-tegn, og 
	%s som inneholder tegn med høy-bit.
 Oppdaget sløyfe i filsystem. %s er en del av samme sløyfe som %s. Filsystem-sløyfe oppdaget. %s har samme enhetsnummer og inode som en mappe som er %d nivå høyere oppe i fil-hierarkiet Filsystem-sløyfe oppdaget. %s har samme enhetsnummer og inode som en mappe som er %d nivåer høyere oppe i fil-hierarkiet Generell hjelp til bruk av GNU-programvare: <http://www.gnu.org/gethelp/>
 Klarte ikke å tolke %s som en dato eller et tidspunkt Ukjent feilsøkingsvalg %s blir ignorert %2$s må brukes alene i %1$s, men du valgte %3$s %s er ugyldig argument for valget «--max-database-age» %s er et ugyldig argument for «-used» %s%s er et ugyldig argument for «-size» Spesifikasjon av inndata-skilletegn inneholder den ugyldige skiftesekvensen %s . Spesifikasjon av inndata-skilletegn inneholder den ugyldige skiftesekvensen %s . Tegnverdier skal ikke overstige %lo. Spesifikasjon av inndata-skilletegn inneholder den ugyldige skiftesekvensen %s . Tegnverdier skal ikke overstige %lx. Spesifikasjon av inndata-skilletegn inneholder den ugyldige skiftesekvensen %s . Etterfølgende tegn %s er ukjent. %s er en ugyldig spesifikasjon av inndata-skilletegn. Skilletegnet må enten være ett tegn eller en skiftesekvens som begynner med «\». %s er et ugyldig optimaliseringsnivå James Youngman Kevin Dalley Størrelse på locate-database: %s byte
 Størrelse på locate-database: %s byte
 Argumenter som er kreves for lange valgnavn kreves også
for tilsvarende korte valgnavn. Det samme gjelder valgfrie argumenter.
 Samsvarende filnavn: %s
 Den gammelformaterte locate-databasen %s er for kort til å være gyldig Du kan bare kjøre én instans av {} med -exec%s … + Optimaliseringsnivå %lu er for høyt. Du bør vurdere å bruke GNU locate hvis du vil finne filer veldig raskt. Pakket av %s
 Pakket av %s (%s)
 Bruk et desimaltall rett etter «-O» Rapporter programfeil i %s til: %s
 Rapporter (og følg behandlingsprosess for) programfeil via feilrapporteringssiden for
findutils på http://savannah.gnu.org/ eller, hvis du mangler web-tilgang, ved å sende
e-post til <bug-findutils@gnu.org>. Kjør valgt KOMMANDO med valgte ARGUMenter og les flere argumenter fra inndata.

 Sikkerhetsnivå %s inneholder uforventet suffiks %s. Sikkerhetsnivå %s er utenfor konverterbar rekkevidde. Enkelte filnavn kan være filtrert ut, så komprimeringsforholdet kan ikke beregnes.
 Symbolsk lenke %s er del av en sløyfe i mappehierarkiet. Du har allerede besøkt mappa den peker på. Testen %s krever at du bruker et argument Valget «-O» må etterfølges av et tall med desimal Valget «-delete» medfører at «-depth» slås på automatisk. «-prune» har ingen betydning når «-depth» er slått på. Hvis du vil fortsette likevel, må du kjøre programmet på nytt og velge «-depth» eksplisitt. valget «-show-control-chars» må argumenteres med enten «literal» eller «safe» Argumentet for valget «--max-database-age» kan ikke stå tomt Argumentet for «-user» kan ikke være tomt atexit-bibliotekfunksjonen mislyktes Gjeldende mappe inngår i miljøvariabelen «PATH». Det er usikkert å bruke dette i kombinasjon med find-valget %s. Fjern gjeldende mappe fra $PATH (det vil si: fjern «.», doble kolon-tegn, ledende og/eller avsluttende kolon-tegn) Databasen har «big-endian machine-word»-koding.
 Databasen har «little-endian machine-word»-koding.
 Databasens «machine-word»-koderekkefølge er ikke åpenbar.
 Miljøet er for stort for exec(). Miljøvariabelen «FIND_BLOCK_SIZE» støttes ikke. Det eneste som påvirker blokkstørrelsen er miljøvariabelen «POSIXLY_CORRECT» Relativ sti %s inngår i miljøvariabelen «PATH». Det er usikkert å bruke dette i kombinasjon med find-valget %s. Fjern dette elementet fra $PATH Dette systemet har ingen tilgjengelig måte å finne fødselstid for filer på. Suffiks %s er uforventet i %s «%c» er et ukjent argument for -type Regulær uttrykkstype %s er ukjent.  Gyldige typer er %s. Ukjent systemfeil Bruk: %s [--version | --help]
eller     %s most_common_bigrams < filliste > locate-database
 Bruk: %s [-0 | --null] [--version] [--help]
 Bruk: %s [-H] [-L] [-P] [-Onivå] [-D  Bruk: %s [-d sti | --database=sti] [-e | -E | --[non-]existing]
      [-i | --ignore-case] [-w | --wholename] [-b | --basename] 
      [--limit=N | -l N] [-S | --statistics] [-0 | --null] [-c | --count]
      [-P | -H | --nofollow] [-L | --follow] [-m | --mmap] [-s | --stdio]
      [-A | --all] [-p | --print] [-r | --regex] [--regextype=TYPE]
      [--max-database-age D] [--version] [--help]
      mønster …
 Bruk: %s [VALG] … KOMMANDO [ARGUM] …
 Følgende argumenter er gyldige: ADVARSEL: Mistet kontroll på %lu underprosesser ADVARSEL: inndata inneholdt NUL-tegn. Dette kan ikke videresendes som et argument. Kanskje valget «--null» er aktuelt? ADVARSEL: klarte ikke å finne fødselstid for fila %s ADVARSEL: fila %s ser ut til å ha modus 0000 ADVARSEL: filsystemet %s ble nylig montert. ADVARSEL: filsystemet %s ble nylig avmontert. ADVARSEL: lokal database %s er bygget opp med en annen byte-rekkefølge Advarsel: %s blir kjørt minst én gang. Trykk avbrudd-tastetrykket hvis du ikke vil at dette skal skje.
 Skrevet av %s og %s.
 Skrevet av %s, %s, %s,
%s, %s, %s, %s,
%s, %s og andre.
 Skrevet av %s, %s, %s,
%s, %s, %s, %s,
%s og %s.
 Skrevet av %s, %s, %s,
%s, %s, %s, %s 
og %s.
 Skrevet av %s, %s, %s,
%s, %s, %s og %s.
 Skrevet av %s, %s, %s,
%s, %s og %s.
 Skrevet av %s, %s, %s,
%s og %s.
 Skrevet av %s, %s, %s 
og %s.
 Skrevet av %s, %s og %s.
 Skrevet av %s.
 Du kan ikke bruke «{}» i verktøynavnet for «-execdir» og «-okdir», fordi det ville skapt et potensielt sikkerhetsproblem. Du må velge sikkerhetsnivå i form av et desimaltall. Du valgte «-E», men dette valget kan ikke brukes med slocate-databaser som har sikkerhetsnivåer utenom null. Ingen søkeresultater blir generert for denne databasen.
 ] [sti …] [uttrykk]
 ^[nN] ^[JjYy] « %s er et flertydig argument for %s argumentlinja er for lang argumentlista er for lang argumentet for «-group» er tomt. Det skal bestå av et gruppenavn det oppstod aritmetisk overbelastning under beregning av når denne dagen slutter det oppstod aritmetisk overbelastning under konvertering av %s dager til antall sekunder klarte ikke å kalle exec() på grunn av restriksjoner mot argumentstørrelse klarte ikke å slette %s får ikke plass til enkeltargument under grensa for argument-listestørrelse klarte ikke å kopiere prosess klarte ikke å søke gjennom %s klarte ikke å samle informasjon om gjeldende mappe kommandoen er for lang klarte ikke å opprette datarør før forgrening dager dobbel miljøet er for stort for å kjøre dette errno-mellomlager safe_read mislyktes i xargs_do_exec (dette er sannsynligvis en programfeil, og vi ber om at du sender inn en feilrapport) feil ved lukking av fil feil ved lesing av ord fra %s programfeil ved venting på %s feil ved venting på underprosess feil: formatstreng slutter på %s feil: formatdirektivet «%%%c» er reservert for fremtidig bruk forventet et uttrykk etter «%s» forventet et uttrykk mellom «%s» og «)» klarte ikke å fjerne grupperettigheter klarte ikke å fjerne setgid-rettigheter klarte ikke å fjerne setuid-rettigheter klarte ikke å lese fra /dev/tty klarte ikke å gjenopprette arbeidsmappe etter å ha søkt gjennom %s klarte ikke å gå tilbake til opprinnelig arbeidsmappe klarte ikke å gå tilbake til foreldermappe klarte ikke å gi miljøvariabelen %s en verdi klarte ikke å tømme miljøvariabelen %s getfilecon mislyktes: %s «%3$s» er et ugyldig argument for %1$s%2$s %c er en ugyldig «-size»-type %s er et ugyldig argument for %s «%s» er et ugyldig argument for «%s» ugyldig uttrykk ugyldig uttrykk. Forventet «)», men fant ingen. ugyldig uttrykk. Tomme parentes tillates ikke. ugyldig uttrykk. Forventet «)», men fant ingen. Det er mulig at du mangler en ekstra påstand etter «%s» ugyldig uttrykk. Du har brukt for mange «)» ugyldig uttrykk. Du har brukt binæroperatøren «%s» uten prefiks. %s er en ugyldig modus ugyldig null-argument for «-size» «-context» kan ikke brukes, fordi SELinux ikke er slått på. «%s» er et ugyldig predikat argumentet «%3$s» for %1$s%2$s inneholder ugyldig suffiks locate-database %s inneholder et filnavn som er lengre enn hva localte kan håndtere locate-database %s er ødelagt eller ugyldig locate-database %s ser ut som en slocate-database, men ser også ut til å ha sikkerhetsnivå %c som GNU findutils ikke støtter per denne versjonen minnet er oppbrukt argument for «%s» mangler ugyldig standardbruk av «and». ugyldig uttrykkstype (%d). ugyldig uttrykkstype. operatører (fallende presedens. «-and» gjelder implisitt hvis intet annet er valgt):
      ( UTTR )   ! UTTR   -not UTTR   UTTR1 -a UTTR2   UTTR1 -and UTTR2
      UTTR1 -o UTTR2   UTTR1 -or UTTR2   UTTR1 , UTTR2
 valget --%s kan ikke ha en verdi som inneholder «=» filstier må angis før uttrykk: %s posisjonsvalg (alltid positiv verdi): -daystart -follow -regextype

vanlige valg (alltid positiv verdi, angitt før andre uttrykk):
      -depth --help -maxdepth NIVÅER -mindepth NIVÅER -mount -noleaf
      --version -xdev -ignore_readdir_race -noignore_readdir_race
 «read» sendte uventet verdi %zu. Dette er sannsynligvis en programfeil, og vi ber om at du sender inn en feilrapport kontroll av bibliotekfunksjonen fnmatch() mislyktes. enkel slocate-sikkerhetsnivå %ld støttes ikke. standardfeilkanal standardutdata tester (N kan være +N, -N eller N): -amin N -anewer FIL -atime N -cmin N
      -cnewer FIL -ctime N -empty -false -fstype TYPE -gid N -group NAVN
      -ilname MØNSTER -iname MØNSTER -inum N -iwholename MØNSTER -iregex MØNSTER
      -links N -lname MØNSTER -mmin N -mtime N -name MØNSTER -newer FIL time-systemkall mislyktes klarte ikke å tildele minne klarte ikke å huske gjeldende arbeidsmappe uforventet EOF i %s uforventet ekstrapredikat ekstrapredikatet «%s» kom uforventet ukjent «%s» er et ukjent predikat ingen treff på %s-sitat. xargs behandler sitattegn som spesialtegn med mindre du bruker valget «-0» advarsel: -%s %s slutter på «/», og samsvarer derfor ikke med noe som helst advarsel: Unix-filnavn inneholder vanligvis ikke skråstreker, selv om filstier gjør det. «%s %s» blir derfor sannsynligvis alltid regnet som falsk på dette systemet. Testene «-wholename» og «-samefile» kan derfor være nyttige. Eventuelt, hvis du bruker GNU grep, kan du bruke «find … -print0 | grep -FzZ %s». advarsel: databasen %s er over %d %s gammel (egentlig alder er %.1f %s) advarsel: skiftetegn «\» etterfulgt av ingenting advarsel: formatdirektivet «%%%c» skal etterfølges av et annet tegn advarsel: symbolsk lenke %s blir ikke fulgt advarsel: valget «-E» har ingen betydning når du bruker «-0» eller «-d».
 advarsel: valget «-d» er utgått. Bruk heller «-depth», som samsvarer med POSIX. advarsel: locate-databasen kan bare leses fra standard innkanal én gang. advarsel: det finnes ingen oppføring i evalueringskost-tabellen for predikat %s. Vi ber om at du rapporterer dette som en programfeil advarsel: «\%c» er en ukjent skiftesekvens advarsel: «%%%c» er et ukjent formatdirektiv advarsel: verdien %ld er for høy for valget «-s». Bruker %ld i stedet advarsel: du har valgt modusmønster %s (som tilsvarer «/000»). Betydningen av «-perm /000» er nå endret til å være ensbetydende med «-perm -000». Det som tidligere ikke samsvarte med noen filer samsvarer altså nå med alle filer. advarsel: du har valgt %s etter argumentet %s (som ikke er knyttet til et valg), men valgene er feilplassert (%s påvirker tester som utføres både før og etter seg selv). Legg inn programvalg før andre argumenter på kommandolinja.
 skrivefeil du har brukt for mange «)» PRIuMAX %s%s changed during execution of %s (old inode number %, new inode number %, file system type is %s) [ref %ld] WARNING: Hard link count is wrong for %s (saw only st_nlink=% but we already saw % subdirectories): this may be a bug in your file system driver.  Automatically turning on find's -noleaf option.  Earlier results may have failed to include directories that should have been searched. Your environment variables take up % bytes
 POSIX upper limit on argument length (this system): %
 POSIX smallest allowable upper limit on argument length (all systems): %
 Maximum length of command we could actually use: %
 Size of command buffer we are actually using: %
 Maximum parallelism (--max-procs must be no greater): %
 %s%s ble endret under kjøring av %s (gammelt inode-nummer: %. Nytt inode-nummer: %. Filsystem-type: %s) [ref %ld] ADVARSEL: Antallet harde lenker er feil for %s (fant bare st_nlink=%, men du har allerede sett % undermapper). Dette kan være en programfeil i filsystem-driveren din. find-valget «-noleaf» er nå slått på automatisk. Tidligere søkeresultater kan mangle enkelte mapper som skulle vært gjennomsøkt. Miljøvariablene dine tar % byte med plass
 Øvre POSIX-grense for argumentlengde (på dette systemet): %
 Laveste tillatte øvre grense for argumentlengde (alle systemer): %
 Maksimal lengde på kommando som faktisk kan brukes: %
 Størrelse på kommando-mellomlager som faktisk brukes: %
 Maksimal parallelitet (må være lik eller høyere enn «--max-procs»): %
 