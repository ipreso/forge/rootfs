��     0   �  {  @     ,     4  T      �  �   �  �   �  )   M     w  �   �  �   �  :     O   J  B   �  C   �     !  �   9  �     �   �  3   �  }   �  I   z   L   �   �   !  �   �!  �   �"  �   P#  O   �#  >   ?$  �   ~$  I   S%  D   �%  E   �%     (&  0   <&  H   m&  N   �&  7   '  "   ='  '   `'  |   �'     (      (  t   =(  $   �(  "   �(     �(  ,   )  ,   C)  ,   p)  '   �)  -   �)      �)  (   *  (   =*     f*     *     �*     �*  *   �*  *   +     1+     3+  _   7+  g   �+  g   �+  h   g,     �,     �,  1   �,     &-  #   B-     f-  $   �-  $   �-  !   �-  !   �-  -   .     <.  !   \.  &   ~.      �.  <   �.     /  >   /     Q/     i/  +   �/     �/  )   �/  @   �/  $   :0     _0  "   ~0     �0     �0  �   �0  �   f1  I   2    d2  ?   s3  9   �3  #   �3  8   4  1   J4     |4      �4  <   �4  b   �4  b   Z5  c   �5  �   !6     �6     �6     �6  >   �6  t   7     �7  6   �7  7   �7  g   8     8     �8  4   �8     �8  �   �8  M   �9  +   �9  3   ":  W   V:  x   �:     ';  ?   E;  �   �;  X   1<  <   �<  )   �<  "   �<  �   =  3   >  6   C>  9   z>  (   �>  �   �>  �   o?  D   @     Y@     t@  7   �@     �@  [   �@  -   ;A  '   iA  �  �A  0   1C     bC  *   wC  �   �C  /   -D  *   ]D  2   �D  4   �D  A   �D  o   2E     �E  ;   �E  3   �E  /   )F  +   YF  '   �F  #   �F     �F     �F     G  q   G  :   �G  �   �G     vH     �H     �H     �H     �H     �H     �H  7   �H  =   !I  C   _I  3   �I     �I  :   �I     #J     /J     @J     ^J  !   oJ     �J     �J  !   �J  Y   �J     K     ,K     IK     ^K  !   ~K  =   �K  !   �K  +    L     ,L      LL      mL  #   �L  6   �L  -   �L  $   M  %   <M  '   bM     �M     �M     �M     �M     �M     N  P   N  6   pN  n   �N  )   O  P   @O     �O     �O  3   �O     �O  $   P  D   0P  (   uP  �   �P     'Q     8Q  )   QQ  %   {Q      �Q  �   �Q  8   �R  !   �R  	  �R  L   �S  6   CT     zT  *   �T     �T     �T  )  �T     �U     V  *   'V     RV     gV     �V     �V     �V  W   �V  ?   W  I  YW  C   �X  .   �X  H   Y  +   _Y  :   �Y  q   �Y  >   8Z  o   wZ  "   �Z  -   
[  @   8[  �   y[  �   [\     A]     M]  �  c]  �   �^  �   �_  o   �`  a   a  �   a  �   ~b  =   c  I   Bc  F   �c  H   �c     d  �   4d    e  �   *f  5   g  ~   Ag  K   �g  I   h  )  Vh  �   �i  �   j  �   �j  O   Dk  A   �k  �   �k  T   �l  >   m  H   Nm     �m  .   �m  T   �m  d   -n  3   �n  *   �n  )   �n  �   o     �o  '   �o  w   �o  '   _p  "   �p     �p  *   �p  0   �p  *   &q  *   Qq  1   |q  %   �q  *   �q  -   �q     -r     Hr     dr     �r  -   �r  ,   �r     �r     �r  [    s  c   \s  c   �s  f   $t     �t     �t  5   �t     �t  +   u  "   -u  .   Pu  /   u  /   �u  /   �u  9   v  ,   Iv      vv  &   �v     �v  >   �v     w  G   +w     sw  7   �w  8   �w  ,   x  1   .x  I   `x  .   �x  .   �x  ,   y  -   5y     cy  �   {y  �   #z  M   �z    {  I   |  G   ]|  &   �|  1   �|  4   �|     3}  '   S}  ?   {}  g   �}  g   #~  f   �~  �   �~     ~     �     �  M   �  |   �     ��  I   ��  0   �  v   �     ��     ��  '   ��  e   ف  D  ?�  Z   ��  .   ߃  :   �  ]   I�  w   ��     �  @   ?�  �   ��  S   1�  =   ��  )   Æ  (   �  �   �  3   �  6   :�  A   q�  "   ��  v   ֈ  �   M�  V   �     A�     [�  7   z�     ��  b   Ê  3   &�  -   Z�  �  ��  8   2�     k�  6   ��  �   ��  7   @�  /   x�  .   ��  0   ׎  =   �  �   F�     ӏ  9   �  2   $�  .   W�  *   ��  &   ��  "   ؐ     ��     �     5�  ~   E�  :   đ  �   ��     ��     Ւ     ے     �     �     �      �  <   >�  =   {�  J   ��  D   �     I�  R   ]�     ��     ��  !   Ӕ     ��  )   �     9�     ?�     G�  o   g�     ו  $   �     �  !   /�  #   Q�  E   u�  #   ��  ,   ߖ  .   �  0   ;�  0   l�  .   ��  K   ̗  <   �  6   U�  *   ��  +   ��     �     ��     �     ;�      X�     y�  b   ��  1   �  �   �  (   ��  R   ɚ     �  !   .�  8   P�     ��  (   ��  V   ɛ  ,    �  �   M�     ݜ     �  0   �  $   4�     Y�  �   y�  F   N�  )   ��    ��  c   ̟  B   0�     s�  )   z�     ��     ��  .  ��     �     	�  &   !�     H�     ^�     v�     ��     ��  u   ��  D   %�  _  j�  K   ʤ  8   �  L   O�  /   ��  >   ̥  g   �  G   s�  �   ��  )   B�  )   l�  L   ��  �   �  �   Ԩ     ��     ȩ     �          H   �   �          �   �   s   {   +   �   �   R       �         B      �   �   �   #           �   r   �   j       
  �   h   6   u   :   �        �       e   �   Z   �          @   v         }   c   �                       \   �   �   |                  p   M   �   �   �   A           �   �   �   <   �   �   �   C   "   3   �      �   �             w   ?          �       �   I     1       �       �   !       �   �              [   �   �   f       �   9   �   x   U      X           (   P              �   
       )   W           �       �   k   ^   �   �          �   �         D         �       �   �         .         �   �      �       4   *   �   �       �   �       y      o   =   �   ~       �   J      /       �   �     -   >       �   	       �                     i   F   �   �   �   K   �   �   �   n                   _   �       �      �          �   N   �     G          �   �   �   �   �               8       �       �   �   �   Q       5           �           	          &   %   �       t   �   �       �   �       �   �       �       Y       a   g   �   `   �       �                �         �   �   �   �   E      �   �                   q   �          ]   �   S                 �   7   �   �         �   �           �              �   T           �   �       �   �     �   z           O   2   L       �   V   �   �   m   �   d           �     l   ;   0   $   �   �       '       �   b          �              ,      ߩ  t  �  �  �  �  �  �    $  @  \  p  �  �  �  �  �  7              $   ����V�  =              �   ����q�  $          ������  5          ����ԫ  H          �����  2          ����R�  /          ������  7          ������  :              "   ����,�  G              �   ����X�            ����}�  =          ������  H          �����  ;          ����C�  :          �����  <          ���� 
Execution of xargs will continue now, and it will try to read its input and run commands; if this is not what you wanted to happen, please type the end-of-file keystroke.
 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to <bug-findutils@gnu.org>.
 
Report bugs to: %s
 
actions: -delete -print0 -printf FORMAT -fprintf FILE FORMAT -print 
      -fprint0 FILE -fprint FILE -ls -fls FILE -prune -quit
      -exec COMMAND ; -exec COMMAND {} + -ok COMMAND ;
      -execdir COMMAND ; -execdir COMMAND {} + -okdir COMMAND ;
 
default path is the current directory; default expression is -print
expression may consist of: operators, options, tests, and actions:
       --help                   display this help and exit
       --process-slot-var=VAR   set environment variable VAR in child processes
       --show-limits            show limits on command-line length
       --version                output version information and exit
       -context CONTEXT
       -nouser -nogroup -path PATTERN -perm [-/]MODE -regex PATTERN
      -readable -writable -executable
      -wholename PATTERN -size N[bcwkMG] -true -type [bcdpflsD] -uid N
      -used N -user NAME -xtype [bcdpfls]   -0, --null                   items are separated by a null, not whitespace;
                                 disables quote and backslash processing and
                                 logical EOF processing
   -E END                       set logical EOF string; if END occurs as a line
                                 of input, the rest of the input is ignored
                                 (ignored if -0 or -d was specified)
   -I R                         same as --replace=R
   -L, --max-lines=MAX-LINES    use at most MAX-LINES non-blank input lines per
                                 command line
   -P, --max-procs=MAX-PROCS    run at most MAX-PROCS processes at a time
   -a, --arg-file=FILE          read arguments from FILE, not standard input
   -d, --delimiter=CHARACTER    items in input stream are separated by CHARACTER,
                                 not by whitespace; disables quote and backslash
                                 processing and logical EOF processing
   -e, --eof[=END]              equivalent to -E END if END is specified;
                                 otherwise, there is no end-of-file string
   -i, --replace[=R]            replace R in INITIAL-ARGS with names read
                                 from standard input; if R is unspecified,
                                 assume {}
   -l[MAX-LINES]                similar to -L but defaults to at most one non-
                                 blank input line if MAX-LINES is not specified
   -n, --max-args=MAX-ARGS      use at most MAX-ARGS arguments per command line
   -p, --interactive            prompt before running commands
   -r, --no-run-if-empty        if there are no arguments, then do not run COMMAND;
                                 if this option is not given, COMMAND will be
                                 run at least once
   -s, --max-chars=MAX-CHARS    limit length of command line to MAX-CHARS
   -t, --verbose                print commands before executing them
   -x, --exit                   exit if the size (see -s) is exceeded
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s is an slocate database of unsupported security level %d; skipping it. %s is an slocate database.  Support for these is new, expect problems for now. %s is an slocate database.  Turning on the '-e' option. %s is not the name of a known user %s is not the name of an existing group %s is not the name of an existing group and it does not look like a numeric group ID because it has the unexpected suffix %s %s terminated by signal %d %s%s argument '%s' too large %s%s changed during execution of %s (old device number %ld, new device number %ld, file system type is %s) [ref %ld] %s: exited with status 255; aborting %s: invalid number for -%c option
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: stopped by signal %d %s: terminated by signal %d %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 %s: value for -%c option should be <= %ld
 %s: value for -%c option should be >= %ld
 ' (C) -type %c is not supported because FIFOs are not supported on the platform find was compiled on. -type %c is not supported because Solaris doors are not supported on the platform find was compiled on. -type %c is not supported because named sockets are not supported on the platform find was compiled on. -type %c is not supported because symbolic links are not supported on the platform find was compiled on. < %s ... %s > ?  All Filenames: %s
 Arguments to -type should contain only one letter Cannot close standard input Cannot obtain birth time of file %s Cannot open input file %s Cannot read list of mounted devices. Cannot read mounted file system list Cannot set SIGUSR1 signal handler Cannot set SIGUSR2 signal handler Compression ratio %4.2f%% (higher is better)
 Compression ratio is undefined
 Database %s is in the %s format.
 Database was last modified at %s.%09ld Empty argument to the -D option. Environment variable %s is not set to a valid decimal number Eric B. Decker Expected a positive decimal integer argument to %s, but got %s Expected an integer: %s Failed to fully drop privileges Failed to initialize shared-file hash table Failed to read from stdin Failed to safely change directory into %s Failed to save working directory in order to run a command on %s Failed to write output (at stage %d) Failed to write prompt for -ok Failed to write to standard output Failed to write to stderr Features enabled:  File descriptor %d will leak; please report this as a bug, remembering to include a detailed description of the simplest way to reproduce this problem. File names have a cumulative length of %s bytes.
Of those file names,

	%s contain whitespace, 
	%s contain newline characters, 
	and %s contain characters with the high bit set.
 File system loop detected; %s is part of the same file system loop as %s. Filesystem loop detected; %s has the same device number and inode as a directory which is %d level higher in the file system hierarchy Filesystem loop detected; %s has the same device number and inode as a directory which is %d levels higher in the file system hierarchy General help using GNU software: <http://www.gnu.org/gethelp/>
 I cannot figure out how to interpret %s as a date or time Ignoring unrecognised debug flag %s In %s the %s must appear by itself, but you specified %s Invalid argument %s for option --max-database-age Invalid argument %s to -used Invalid argument `%s%s' to -size Invalid escape sequence %s in input delimiter specification. Invalid escape sequence %s in input delimiter specification; character values must not exceed %lo. Invalid escape sequence %s in input delimiter specification; character values must not exceed %lx. Invalid escape sequence %s in input delimiter specification; trailing characters %s not recognised. Invalid input delimiter specification %s: the delimiter must be either a single character or an escape sequence starting with \. Invalid optimisation level %s James Youngman Kevin Dalley Locate database size: %s byte
 Locate database size: %s bytes
 Mandatory and optional arguments to long options are also
mandatory or optional for the corresponding short option.
 Matching Filenames: %s
 Old-format locate database %s is too short to be valid Only one instance of {} is supported with -exec%s ... + Optimisation level %lu is too high.  If you want to find files very quickly, consider using GNU locate. Packaged by %s
 Packaged by %s (%s)
 Please specify a decimal number immediately after -O Report %s bugs to: %s
 Report (and track progress on fixing) bugs via the findutils bug-reporting
page at http://savannah.gnu.org/ or, if you have no web access, by sending
email to <bug-findutils@gnu.org>. Run COMMAND with arguments INITIAL-ARGS and more arguments read from input.

 Security level %s has unexpected suffix %s. Security level %s is outside the convertible range. Some filenames may have been filtered out, so we cannot compute the compression ratio.
 Symbolic link %s is part of a loop in the directory hierarchy; we have already visited the directory to which it points. The %s test needs an argument The -O option must be immediately followed by a decimal integer The -delete action automatically turns on -depth, but -prune does nothing when -depth is in effect.  If you want to carry on anyway, just explicitly use the -depth option. The -show-control-chars option takes a single argument which must be 'literal' or 'safe' The argument for option --max-database-age must not be empty The argument to -user should not be empty The atexit library function failed The current directory is included in the PATH environment variable, which is insecure in combination with the %s action of find.  Please remove the current directory from your $PATH (that is, remove ".", doubled colons, or leading or trailing colons) The database has big-endian machine-word encoding.
 The database has little-endian machine-word encoding.
 The database machine-word encoding order is not obvious.
 The environment is too large for exec(). The environment variable FIND_BLOCK_SIZE is not supported, the only thing that affects the block size is the POSIXLY_CORRECT environment variable The relative path %s is included in the PATH environment variable, which is insecure in combination with the %s action of find.  Please remove that entry from $PATH This system does not provide a way to find the birth time of a file. Unexpected suffix %s on %s Unknown argument to -type: %c Unknown regular expression type %s; valid types are %s. Unknown system error Usage: %s [--version | --help]
or     %s most_common_bigrams < file-list > locate-database
 Usage: %s [-0 | --null] [--version] [--help]
 Usage: %s [-H] [-L] [-P] [-Olevel] [-D  Usage: %s [-d path | --database=path] [-e | -E | --[non-]existing]
      [-i | --ignore-case] [-w | --wholename] [-b | --basename] 
      [--limit=N | -l N] [-S | --statistics] [-0 | --null] [-c | --count]
      [-P | -H | --nofollow] [-L | --follow] [-m | --mmap] [-s | --stdio]
      [-A | --all] [-p | --print] [-r | --regex] [--regextype=TYPE]
      [--max-database-age D] [--version] [--help]
      pattern...
 Usage: %s [OPTION]... COMMAND [INITIAL-ARGS]...
 Valid arguments are: WARNING: Lost track of %lu child processes WARNING: a NUL character occurred in the input.  It cannot be passed through in the argument list.  Did you mean to use the --null option? WARNING: cannot determine birth time of file %s WARNING: file %s appears to have mode 0000 WARNING: file system %s has recently been mounted. WARNING: file system %s has recently been unmounted. WARNING: locate database %s was built with a different byte order Warning: %s will be run at least once.  If you do not want that to happen, then press the interrupt keystroke.
 Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 You may not use {} within the utility name for -execdir and -okdir, because this is a potential security problem. You need to specify a security level as a decimal integer. You specified the -E option, but that option cannot be used with slocate-format databases with a non-zero security level.  No results will be generated for this database.
 ] [path...] [expression]
 ^[nN] ^[yY] ` ambiguous argument %s for %s argument line too long argument list too long argument to -group is empty, but should be a group name arithmetic overflow when trying to calculate the end of today arithmetic overflow while converting %s days to a number of seconds can't call exec() due to argument size restrictions cannot delete %s cannot fit single argument within argument list size limit cannot fork cannot search %s cannot stat current directory command too long could not create pipe before fork days double environment is too large for exec errno-buffer safe_read failed in xargs_do_exec (this is probably a bug, please report it) error closing file error reading a word from %s error waiting for %s error waiting for child process error: %s at end of format string error: the format directive `%%%c' is reserved for future use expected an expression after '%s' expected an expression between '%s' and ')' failed to drop group privileges failed to drop setgid privileges failed to drop setuid privileges failed to open /dev/tty for reading failed to restore working directory after searching %s failed to return to initial working directory failed to return to parent directory failed to set environment variable %s failed to unset environment variable %s getfilecon failed: %s invalid %s%s argument '%s' invalid -size type `%c' invalid argument %s for %s invalid argument `%s' to `%s' invalid expression invalid expression; I was expecting to find a ')' somewhere but did not see one. invalid expression; empty parentheses are not allowed. invalid expression; expected to find a ')' but didn't see one.  Perhaps you need an extra predicate after '%s' invalid expression; you have too many ')' invalid expression; you have used a binary operator '%s' with nothing before it. invalid mode %s invalid null argument to -size invalid predicate -context: SELinux is not enabled. invalid predicate `%s' invalid suffix in %s%s argument '%s' locate database %s contains a filename longer than locate can handle locate database %s is corrupt or invalid locate database %s looks like an slocate database but it seems to have security level %c, which GNU findutils does not currently support memory exhausted missing argument to `%s' oops -- invalid default insertion of and! oops -- invalid expression type (%d)! oops -- invalid expression type! operators (decreasing precedence; -and is implicit where no others are given):
      ( EXPR )   ! EXPR   -not EXPR   EXPR1 -a EXPR2   EXPR1 -and EXPR2
      EXPR1 -o EXPR2   EXPR1 -or EXPR2   EXPR1 , EXPR2
 option --%s may not be set to a value which includes `=' paths must precede expression: %s positional options (always true): -daystart -follow -regextype

normal options (always true, specified before other expressions):
      -depth --help -maxdepth LEVELS -mindepth LEVELS -mount -noleaf
      --version -xdev -ignore_readdir_race -noignore_readdir_race
 read returned unexpected value %zu; this is probably a bug, please report it sanity check of the fnmatch() library function failed. single slocate security level %ld is unsupported. standard error standard output tests (N can be +N or -N or N): -amin N -anewer FILE -atime N -cmin N
      -cnewer FILE -ctime N -empty -false -fstype TYPE -gid N -group NAME
      -ilname PATTERN -iname PATTERN -inum N -iwholename PATTERN -iregex PATTERN
      -links N -lname PATTERN -mmin N -mtime N -name PATTERN -newer FILE time system call failed unable to allocate memory unable to record current working directory unexpected EOF in %s unexpected extra predicate unexpected extra predicate '%s' unknown unknown predicate `%s' unmatched %s quote; by default quotes are special to xargs unless you use the -0 option warning: -%s %s will not match anything because it ends with /. warning: Unix filenames usually don't contain slashes (though pathnames do).  That means that '%s %s' will probably evaluate to false all the time on this system.  You might find the '-wholename' test more useful, or perhaps '-samefile'.  Alternatively, if you are using GNU grep, you could use 'find ... -print0 | grep -FzZ %s'. warning: database %s is more than %d %s old (actual age is %.1f %s) warning: escape `\' followed by nothing at all warning: format directive `%%%c' should be followed by another character warning: not following the symbolic link %s warning: the -E option has no effect if -0 or -d is used.
 warning: the -d option is deprecated; please use -depth instead, because the latter is a POSIX-compliant feature. warning: the locate database can only be read from stdin once. warning: there is no entry in the predicate evaluation cost table for predicate %s; please report this as a bug warning: unrecognized escape `\%c' warning: unrecognized format directive `%%%c' warning: value %ld for -s option is too large, using %ld instead warning: you have specified a mode pattern %s (which is equivalent to /000). The meaning of -perm /000 has now been changed to be consistent with -perm -000; that is, while it used to match no files, it now matches all files. warning: you have specified the %s option after a non-option argument %s, but options are not positional (%s affects tests specified before it as well as those specified after it).  Please specify options before other arguments.
 write error you have too many ')' Project-Id-Version: findutils 4.5.15
Report-Msgid-Bugs-To: bug-findutils@gnu.org
POT-Creation-Date: 2015-12-27 20:34+0000
PO-Revision-Date: 2015-12-18 21:52+0100
Last-Translator: Göran Uddeborg <goeran@uddeborg.se>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
Körningen av xargs kommer att fortsätta nu och den kommer att försöka läsa dess inmatning och köra kommandon; om det här inte är vad du ville skulle hända kan du trycka på tangentkombinationen för filslut.
 
Licens GPLv3+: GNU GPL version 3 eller senare <http://gnu.org/licenses/gpl.html>.
Detta är fri programvara: du får fritt ändra och vidaredistribuera den.
Det finns INGEN GARANTI, så långt lagen tillåter.

 
Rapportera fel till <bug-findutils@gnu.org>
och synpunkter på översättningen till <tp-sv@listor.tp-sv.se>.
 
Rapportera fel till: %s
Rapportera synpunkter på översättningen till <tp-sv@listor.tp-sv.se>
 
åtgärder: -delete -print0 -printf FORMAT -fprintf FIL FORMAT -print 
      -fprint0 FIL -fprint FIL -ls -fls FIL -prune -quit
      -exec KOMMANDO ; -exec KOMMANDO {} + -ok KOMMANDO ;
      -execdir KOMMANDO ; -execdir KOMMANDO {} + -okdir KOMMANDO ;
 
standardsökväg är aktuell katalog; standarduttryck är -print
uttryck kan bestå av: operatorer, flaggor, tester och åtgärder:
       --help                   visa denna hjälp och avsluta
       --process-slot-var=VAR   sätt miljövariabeln VAR i barnprocesser
       --show-limits            visa gränser på kommandoradslängden
       --version                skriv ut versionsinformation och avsluta
       -context KONTEXT
       -nouser -nogroup -path MÖNSTER -perm [-/]RÄTTIGHET -regex MÖNSTER
      -readable -writable -executable
      -wholename MÖNSTER -size N[bcwkMG] -true -type [bcdpflsD] -uid N
      -used N -user NAMN -xtype [bcdpfls]   -0, --null                   saker avgränsas av noll, inte blanktecken;
                                 inaktiverar behandling av citationstecken och
                                 omvänt snedstreck samt behandling av logiskt
                                 filslut
   -E SLUT                      sätt logisk SLUTsträng; om SLUT förekommer som
                                 en rad i indata ignoreras resten av indata
                                 (ignoreras om -0 eller -d angavs)
   -I E                         samma som --replace=E
   -L, --max-lines=MAX-RADER    använd högst MAX-RADER ej tomma indatarader per
                                 kommandorad
   -P, --max-procs=MAX-PROCS    kör högst MAX-PROCS processer åt gången
   -a, --arg-file=FIL           läs argument från FIL, inte standard in
   -d, --delimiter=TECKEN       saker i indataströmmen avgränsas av TECKEN, inte
                                 av blanktecken; avaktiverar behandling av
                                 citationstecken och omvänt snedstreck samt
                                 behandling av logiskt filslut
   -e, --eof[=SLUT]             detsamma som -E SLUT om SLUT anges;
                                 annars så finns det ingen filslutssträng
   -i, --replace[=E]            ersätt E i STARTARGUMENT med namn lästa från
                                 standard in; om R utelämnas, anta {}
   -l[MAX-RADER]                snarlikt -L men har som standardvärde högst en
                                 icke tom indatarad om MAX-RADER inte anges
   -n, --max-args=MAX-ARG       använd högst MAX-ARG argument per kommandorad
   -p, --interactive            fråga före varje kommando körs
   -r, --no-run-if-empty        om det inte finns några argument, kör då inte
                                 KOMMANDO; om denna flagga inte anges kommer
                                 KOMMANDO köras åtminstone en gång
   -s, --max-chars=MAX-TECKEN   begränsa längden på kommandoraden till MAX-TECKEN
   -t, --verbose                skriv kommandon före de körs
   -x, --exit                   avsluta om storleken (se -s) överskrids
 %s hemsida: <%s>
 %s hemsida: <http://www.gnu.org/software/%s/>
 %s är en slocate-databas med säkerhetsnivån %d som inte stöds; hoppar över den. %s är en slocate-databas.  Stödet för dessa är nytt så förvänta dig problem för tillfället. %s är en slocate-databas.  Slår på flaggan "-e". %s är inte namnet på en känd användare %s är inte namnet på en befintlig grupp %s är inte namnet på en befintlig grupp och det ser inte ut som ett numeriskt grupp-id därför att det har den oväntade ändelsen %s %s avslutades av signal %d %s%s-argumentet ”%s” är för stort %s%s ändrades under körning av %s (gammalt enhetsnummer %ld, nytt enhetsnummer %ld, filsystemstypen är %s) [ref %ld] %s: avslutades med status 255; avbryter %s: ogiltigt tal för flaggan -%c
 %s: ogiltig flagga -- ”%c”
 %s: flaggan ”%c%s” tar inget argument
 %s: flaggan ”%s” är tvetydig; möjligheter: %s: flaggan ”--%s” tar inget argument
 %s: flaggan ”%s” kräver ett argument
 %s: flaggan ”-W %s” tillåter inget argument
 %s: flaggan ”-W %s” är tvetydig
 %s: flaggan ”%s” kräver ett argument
 %s: flaggan kräver ett argument -- ”%c”
 %s: stoppades av signal %d %s: avslutades av signal %d %s: okänd flagga ”%c%s”
 %s: okänd flagga ”--%s”
 %s: värdet på flaggan -%c ska vara ≤ %ld
 %s: värdet på flaggan -%c ska vara >= %ld
 " © -type %c stödjs inte eftersom FIFO:er inte stödjs på plattformen där find kompilerades. -type %c stödjs inte eftersom Solaris-dörrar inte stödjs på plattformen där find kompilerades. -type %c stödjs inte eftersom namngivna uttag inte stödjs på plattformen där find kompilerades. -type %c stödjs inte eftersom symboliska länkar inte stödjs på plattformen där find kompilerades. < %s ... %s > ?  Alla filnamn: %s
 Argument till -type bör endast innehålla en bokstav Kan inte stänga standard in Kan inte läsa av födelsetid för filen %s Kan inte öppna inmatningsfilen %s Kan inte läsa listan över monterade enheter. Kan inte läsa listan över monterade filsystem Kan inte sätta en signalhanterare för SIGUSR1 Kan inte sätta en signalhanterare för SIGUSR2 Komprimeringsförhållande %4.2f% % (högre är bättre)
 Komprimeringsförhållandet är odefinierat
 Databasen %s är i formatet %s.
 Databasen modifierades senast %s.%09ld Tomt argument till flaggan -D. Miljövariabeln %s är inte satt till ett giltigt decimalt tal Eric B. Decker Förväntade ett positivt decimalt heltalsargument till %s, men fick %s Ett heltal förväntades: %s Misslyckades med att fullständigt släppa rättigheter Misslyckades att initiera hash-tabell över delade filer Misslyckades med att läsa från standard in Misslyckades med att säkert byta katalog till %s Misslyckades att spara arbetskatalogen för att köra ett kommando på %s Misslyckades med att skriva utdata (i steg %d) Misslyckades med att skriva en prompt för -ok Misslyckades med att skriva till standard ut Misslyckades med att skriva till standard fel Aktiverade funktioner:  Filbeskrivaren %d kommer läcka; rapportera detta som ett fel, och kom ihåg att ta med en detaljerad beskrivning av det enklaste sättet att återskapa detta problem. Filnamnen har en kumulativ längd på %s byte.
Av dessa filnamn innehåller

	%s tomrum, 
	%s nyradstecken och 
	%s tecken med den höga biten satt.
 Filsystemsslinga upptäcktes; %s är en del av samma filsystemsslinga som %s. Filsystemsslinga upptäcktes; %s har samma enhetsnummer och inod som en katalog vilken är %d nivå högre upp i filsystemshierarkin Filsystemsslinga upptäcktes; %s har samma enhetsnummer och inod som en katalog vilken är %d nivåer högre upp i filsystemshierarkin Allmän hjälp i att använda GNU-program: <http://www.gnu.org/gethelp/>
 Jag kan inte lista ut om jag ska tolka %s som ett datum eller klockslag Ignorerar okänd felsökningsflagga %s I %s måste %s förekomma ensamt, men du angav %s Ogiltigt argument %s för flaggan --max-database-age Ogiltigt argument %s till -used Ogiltigt argument ”%s%s” till -size Ogiltig specialsekvens %s i inmatningsavskiljarspecifikationen. Ogiltig specialsekvens %s i inmatningsavskiljarspecifikationen; teckenvärden får inte överstiga %lo. Ogiltig specialsekvens %s i inmatningsavskiljarspecifikationen; teckenvärden får inte överstiga %lx. Ogiltig specialsekvens %s i inmatningsavskiljarspecifikationen; efterföljande tecknen %s är okända. Ogiltig inmatningsavskiljarspecifikation %s: avskiljaren måste vara antingen ett enstaka tecken eller en specialsekvens som börjar med \. Ogiltig optimeringsnivå %s James Youngman Kevin Dalley Storlek på locate-databasen: %s byte
 Storlek på locate-databasen: %s byte
 Obligatoriska och valfria argument till långa flaggor är obligatoriska
eller valfria även för motsvarande korta flagga.
 Matchande filnamn: %s
 locate-databasen %s med gammalt format är för kort för att vara giltig Endast en instans av {} stöds med -exec%s ... + Optimeringsnivån %lu är för hög.  Om du mycket snabbt vill hitta filer bör du överväga att använda GNU locate. Paketerat av %s
 Paketerat av %s (%s)
 Ange ett decimaltal omedelbart efter -O Rapportera fel i %s till: %s
Rapportera synpunkter på översättningen till <tp-sv@listor.tp-sv.se>
 Rapportera (och följ rättningen av) fel genom felrapporteringssidan för
findutils på http://savannah.gnu.org/ eller, om du inte har tillgång till
webben, genom att skicka e-post till <bug-findutils@gnu.org>. Skriv
felrapporter på engelska om möjligt.
Skicka synpunkter på översättningen till tp-sv@listor.tp-sv.se. Kör KOMMANDO med argumenten STARTARGUMENT och ytterligare argument lästa från indata.

 Säkerhetsnivån %s har oväntade suffixet %s. Säkerhetsnivån %s är utanför konverteringsintervallet. Några filnamn kan ha filtrerats bort, så vi kan inte beräkna komprimeringsförhållandet.
 Symboliska länken %s är en del av en slinga i kataloghierarkin; vi har redan besökt katalogen till vilken den pekar. %s-testet behöver ett argument Flaggan -O måste omedelbart efterföljas av ett decimalt heltal Åtgärden -delete slår automatiskt på -depth, men -prune gör ingenting när -depth är aktiverat.  Om du vill fortsätta ändå kan du uttryckligen använda flaggan -depth. Flaggan -show-control-chars tar ett argument som måste vara "literal" eller "safe" Argumentet för flaggan --max-database-age får inte vara tom Argumentet till -user får inte vara tomt Biblioteksfunktionen atexit misslyckades Den aktuella katalogen är inkluderad i miljövariabeln PATH, vilket är osäkert i kombination med åtgärden %s för find. Ta bort aktuell katalog från din $PATH (alltså, ta bort ”.”, dubbla kolon, inledande eller avslutande kolon) Databasen har ordkodning för big-endian-maskiner.
 Databasen har ordkodning för little-endian-maskiner.
 Ordningen för databasens maskinordskodning är inte självklar.
 Miljön är för stor för exec(). Miljövariabeln FIND_BLOCK_SIZE stöds inte, det enda som påverkar blockstorleken är miljövariabeln POSIXLY_CORRECT Relativa sökvägen %s är inkluderad i miljövariabeln PATH, vilket är osäkert i kombination med åtgärden %s för find.  Ta bort den posten från $PATH Det här systemet tillhandahåller inte ett sätt att hitta födelsetiden för en fil. Oväntat suffix %s på %s Okänt argument till -type: %c Okänd reguljärt uttryckstyp %s; giltiga typer är %s. Okänt systemfel Användning: %s [--version | --help]
eller       %s vanligaste_bigram < fillista > locate-databas
 Användning: %s [-0 | --null] [--version] [--help]
 Användning: %s [-H] [-L] [-P] [-Onivå] [-D  Användning: %s [-d path | --database=sökväg] [-e | -E | --[non-]existing]
      [-i | --ignore-case] [-w | --wholename] [-b | --basename] 
      [--limit=N | -l N] [-S | --statistics] [-0 | --null] [-c | --count]
      [-P | -H | --nofollow] [-L | --follow] [-m | --mmap] [-s | --stdio]
      [-A | --all] [-p | --print] [-r | --regex] [--regextype=TYP]
      [--max-database-age D] [--version] [--help]
      mönster…
 Användning: %s [FLAGGA]… KOMMANDO [STARTARGUMENT]…
 Giltiga argument är: VARNING: Förlorade kontrollen över %lu barnprocesser VARNING: ett NOLL-tecken förekommer i inmatningen.  Det kan inte skickas vidare i argumentlistan.  Tänkte du använda flaggan --null? VARNING: kan inte bestämma födelsetiden för filen %s VARNING: filen %s verkar ha rättigheterna 0000 VARNING: filsystemet %s har nyligen monterats. VARNING: filsystemet %s har nyligen avmonterats. VARNING: locate-databasen %s byggdes med en annan byteordning Varning: %s kommer att köras minst en gång.  Om du inte vill att det här ska hända kan du trycka på tangentkombinationen för avbrott.
 Skrivet av %s och %s.
 Skrivet av %s, %s, %s,
%s, %s, %s, %s,
%s, %s med flera.
 Skrivet av %s, %s, %s,
%s, %s, %s, %s,
%s och %s.
 Skrivet av %s, %s, %s,
%s, %s, %s, %s
och %s.
 Skrivet at %s, %s, %s,
%s, %s, %s och %s.
 Skrivet av %s, %s, %s,
%s, %s och %s.
 Skrivet av %s, %s, %s,
%s och %s.
 Skrivet av %s, %s, %s
och %s.
 Skrivet av %s, %s och %s.
 Skrivet av %s.
 Du kan inte använda {} inom verktygsnamnet för -execdir och -okdir, på grund av att det innebär en möjlig säkerhetsrisk. Du måste ange en säkerhetsnivå som ett decimalt heltal. Du angav flaggan -E men den flaggan kan inte användas med databaser med slocate-format med en säkerhetsnivå högre än 0.  Inga resultat kommer att genereras för den här databasen.
 ] [sökväg...] [uttryck]
 ^[nN] ^[jJyY] " tvetydigt argument %s för %s argumentraden är för lång argumentlistan är för lång argumentet till -group är tomt, men bör vara ett gruppnamn aritmetiskt överflöde vid försök att beräkna dagens slut aritmetiskt spill vid vid konvertering av %s dagar till ett antal sekunder kan inte anropa exec() på grund av begränsningar i argumentstorlek kan inte ta bort %s får inte plats med ett ensamt argument inom gränsen för argumentlistans storlek kan inte grena kan inte söka i %s kan inte läsa av aktuell katalog kommandot är för långt kunde inte skapa ett rör före grenandet dagar dubbelt miljön är för stor för exec safe_read av errno-buffert misslyckades i xargs_do_exec (detta är förmodligen ett fel, rapportera det gärna) fel vid stängning av fil fel vid läsning av ett ord från %s fel vid väntande på %s fel vid väntande på barnprocess fel: %s på slutet av formatsträng fel: formatdirektivet "%%%c" är reserverat för framtida användning förväntade ett uttryck efter "%s" förväntade ett uttryck mellan "%s" och ")" misslyckades med att släppa grupprättigheter misslyckades med att släppa setgid-rättigheter misslyckades med att släppa setuid-rättigheter misslyckades att öppna /dev/tty för läsning misslyckades med att återställa arbetskatalogen efter genomsökning av %s misslyckades med att återgå till ursprunglig arbetskatalog misslyckades med att återgå till föräldrakatalogen misslyckades att sätta miljövariabeln %s misslyckades att ta bort miljövariabeln %s getfilecon misslyckades: %s ogiltigt %s%s-argument ”%s” ogiltig typ "%c" för -size ogiltigt argument %s för %s ogiltigt argument "%s" till "%s" ogiltigt uttryck ogiltigt uttryck; Jag förväntade mig att hitta ett ")"-tecken någonstans men kunde inte se ett. ogiltigt uttryck; tomma parenteser tillåts inte. ogiltigt uttryck; förväntade att hitta ett ")"-tecken men kunde inte se ett.  Kanske behöver du ett extra predikat efter "%s" ogiltigt uttryck; du har för många ")" ogiltigt uttryck; du har använt en binäroperator "%s" utan någonting före den. ogiltigt läge %s tomt argument till -size ogiltigt felaktigt predikat -context: SELinux är inte aktiverat. ogiltigt predikat "%s" ogiltigt suffix i %s%s-argument ”%s” locate-databasen %s innehåller ett filnamn som är längre än vad locate kan hantera locate-databasen %s är skadad eller ogiltig locate-databasen %s ser ut som en slocate-databas men den verkar ha säkerhetsnivån %c, som GNU findutils för närvarande inte har stöd för minnet slut argument till "%s" saknas hoppsan -- ogiltig standardinsättning av "and"! hoppsan -- ogiltig uttryckstyp (%d)! hoppsan -- ogiltig uttryckstyp! operatorer (avtagande prioritetsordning; -and underförstås när inga andra
anges):
      ( UTTR )   ! UTTR   -not UTTR   UTTR1 -a UTTR2   UTTR1 -and UTTR2
      UTTR1 -o UTTR2   UTTR1 -or UTTR2   UTTR1 , UTTR2
 flaggan --%s får inte sättas till ett värde som innehåller ”=” sökvägar måste komma före uttryck: %s positionsberoende flaggor (alltid sanna): -daystart -follow -regextype

normala flaggor (alltid sanna, anges före andra uttryck):
      -depth --help -maxdepth NIVÅER -mindepth NIVÅER -mount -noleaf
      --version -xdev -ignore_readdir_race -noignore_readdir_race
 läsning returnerade ett oväntat värde %zu; detta är förmodligen ett fel, rapportera det gärna rimlighetskontroll av biblioteksfunktionen fnmatch() misslyckades. enkelt slocates säkerhetsnivå %ld stöds inte. standard fel standard ut tests (N kan vara +N, -N eller N): -amin N -anewer FIL -atime N -cmin N
      -cnewer FIL -ctime N -empty -false -fstype TYP -gid N -group NAMN
      -ilname MÖNSTER -iname MÖNSTER -inum N -iwholename MÖNSTER -iregex MÖNSTER
      -links N -lname MÖNSTER -mmin N -mtime N -name MÖNSTER -newer FIL tidsystemsanrop misslyckades kan inte allokera minne kunde inte spara aktuell arbetskatalog oväntat filslut i %s oväntat extra predikat oväntat extra predikat "%s" okänd okänt predikat "%s" citattecknet %s är oavslutat; som standard är citattecken speciella för xargs såvida du inte använder flaggan -0 varning: -%s %s kommer inte matcha något eftersom det slutar med /. varning: Unix-filnamn innehåller normalt sett inte snedstreck (även om sökvägar gör det).  Det betyder att "%s %s" antagligen kommer alltid att bli falsk på detta system.  Du kanske tycker testet "-wholename" är användbart, eller kanske "-samefile".  Alternativt, om du använder GNU grep, kunde du använda "'find ... -print0 | grep -FzZ %s". varning: databasen %s är mer än %d %s gammal (faktisk ålder är %.1f %s) varning: kontrollsekvensen ”\” följs inte av något varning: formatdirektivet ”%%%c” skall följas av ett ytterligare tecken varning: följer inte den symboliska länken %s varning: flaggan -E har ingen effekt om -0 eller -d används.
 varning: flaggan -d är föråldrad; använd -depth istället, eftersom den senare stöds enligt POSIX. varning: locate-databasen kan endast läsas en gång från standard in. varning: det finns ingen post i tabellen över kostnad för predikatberäkning för predikatet %s; rapportera gärna detta som ett fel varning: okänd kontrollsekvens ”\%c” varning: okänt formatdirektiv ”%%%c” varning: värdet %ld för flaggan -s är för stort, använder %ld istället varning: du måste ange ett lägesmönster %s (som är likvärdigt med /000). Betydelsen av -perm /000 kommer har nu ändrats för att överensstämma med -perm 000; alltså, det brukade inte matcha några fil men nu matchar det alla filer. varning: du har angivit flaggan %s efter argumentet %s som inte är en flagga, men flaggor beror inte på position (%s påverkar test som är angivna före den, liksom de som är angivna efter den). Ange flaggor före andra argument.
 skrivfel du har för många ")" PRIuMAX %s%s changed during execution of %s (old inode number %, new inode number %, file system type is %s) [ref %ld] WARNING: Hard link count is wrong for %s (saw only st_nlink=% but we already saw % subdirectories): this may be a bug in your file system driver.  Automatically turning on find's -noleaf option.  Earlier results may have failed to include directories that should have been searched. Your environment variables take up % bytes
 POSIX upper limit on argument length (this system): %
 POSIX smallest allowable upper limit on argument length (all systems): %
 Maximum length of command we could actually use: %
 Size of command buffer we are actually using: %
 Maximum parallelism (--max-procs must be no greater): %
 %s%s ändrades under körning av %s (gammalt inodsnummer %, nytt inodsnummer %, filsystemstyp är %s) [ref %ld] VARNING: Antalet hårda länkar är fel för %s (såg endast st_nlink=% men vi har redan sett % underkataloger): detta kan vara ett fel i din filsystemdrivrutin.  Slår automatiskt på flaggan -noleaf i find.  Tidigare resultat kan ha misslyckats att inkludera kataloger som skulle ha sökts igenom. Dina miljövariabler tar upp % byte
 Övre POSIX-gräns för argumentlängd (det här systemet): %
 Minsta tillåtna övre POSIX-gräns för argumentlängd (alla system): %
 Maximal längd på kommando som vi faktiskt kan använda: %
 Storlek på kommandobufferten som vi faktiskt använder: %
 Maximal parallellism (--max-procs får inte vara större): %
 