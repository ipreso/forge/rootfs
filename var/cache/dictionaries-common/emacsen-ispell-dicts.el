;;; This file is part of the dictionaries-common package.
;;; It has been automatically generated.
;;; DO NOT EDIT!

;; Adding aspell dicts

(add-to-list 'debian-aspell-only-dictionary-alist
  '("francais-sml"
     "[A-Za-z��������������������������]"
     "[^A-Za-z��������������������������]"
     "[-']"
     t
     ("-d" "fr_FR-sml")
     "~list"
     iso-8859-1))
(add-to-list 'debian-aspell-only-dictionary-alist
  '("francais-lrg"
     "[A-Za-z��������������������������]"
     "[^A-Za-z��������������������������]"
     "[-']"
     t
     ("-d" "fr_FR-lrg")
     "~list"
     iso-8859-1))
(add-to-list 'debian-aspell-only-dictionary-alist
  '("francais-ch"
     "[A-Za-z��������������������������]"
     "[^A-Za-z��������������������������]"
     "[-']"
     t
     ("-d" "fr_CH-60")
     "~list"
     iso-8859-1))
(add-to-list 'debian-aspell-only-dictionary-alist
  '("francais"
     "[A-Za-z��������������������������]"
     "[^A-Za-z��������������������������]"
     "[-']"
     t
     ("-d" "francais")
     "~list"
     iso-8859-1))


;; Adding ispell dicts

(add-to-list 'debian-ispell-only-dictionary-alist
  '("francais-tex8b"
     "[A-Za-z������������ܼ��������������]"
     "[^A-Za-z������������ܼ��������������]"
     "[-']"
     t
     ("-d" "francais-TeX8b")
     "~list"
     iso-8859-15))
(add-to-list 'debian-ispell-only-dictionary-alist
  '("francais-latin1"
     "[A-Za-z��������������������������]"
     "[^A-Za-z��������������������������]"
     "[-']"
     t
     ("-d" "francais")
     "~latin1"
     iso-8859-1))
(add-to-list 'debian-ispell-only-dictionary-alist
  '("francais"
     "[A-Za-z������������ܼ��������������]"
     "[^A-Za-z������������ܼ��������������]"
     "[-']"
     t
     ("-d" "francais")
     "~list"
     iso-8859-15))



;; No emacsen-aspell-equivs entries were found


;; No emacsen-hunspell-equivs entries were found
