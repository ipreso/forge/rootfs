%%% This file is part of the dictionaries-common package.
%%% It has been automatically generated.
%%% DO NOT EDIT!

#ifexists aspell_add_dictionary
  if (_slang_utf8_ok) {
    aspell_add_dictionary (
      "francais",
      "francais",
      "ÀÂÇÈÉÊËÎÏÔÙÛÜàâçèéêëîïôùûü",
      "-'",
      "");
    aspell_add_dictionary (
      "francais-ch",
      "fr_CH-60",
      "ÀÂÇÈÉÊËÎÏÔÙÛÜàâçèéêëîïôùûü",
      "-'",
      "");
    aspell_add_dictionary (
      "francais-lrg",
      "fr_FR-lrg",
      "ÀÂÇÈÉÊËÎÏÔÙÛÜàâçèéêëîïôùûü",
      "-'",
      "");
    aspell_add_dictionary (
      "francais-sml",
      "fr_FR-sml",
      "ÀÂÇÈÉÊËÎÏÔÙÛÜàâçèéêëîïôùûü",
      "-'",
      "");
  } else {
  aspell_add_dictionary (
    "francais",
    "francais",
    "��������������������������",
    "-'",
    "");
  aspell_add_dictionary (
    "francais-ch",
    "fr_CH-60",
    "��������������������������",
    "-'",
    "");
  aspell_add_dictionary (
    "francais-lrg",
    "fr_FR-lrg",
    "��������������������������",
    "-'",
    "");
  aspell_add_dictionary (
    "francais-sml",
    "fr_FR-sml",
    "��������������������������",
    "-'",
    "");
  }
#endif

#ifexists ispell_add_dictionary
  ispell_add_dictionary (
    "francais",
    "francais",
    "������������ܼ��������������",
    "-'",
    "~list",
    "");
  ispell_add_dictionary (
    "francais-latin1",
    "francais",
    "��������������������������",
    "-'",
    "~latin1",
    "");
  ispell_add_dictionary (
    "francais-tex8b",
    "francais-TeX8b",
    "������������ܼ��������������",
    "-'",
    "~list",
    "");
#endif
